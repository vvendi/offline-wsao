# Introduction:
This repository contains the code to reproduce the work of main paper: *link to come*
It contains the data and code to run the 3 steps of offline walsh surrogate assisted optimization:
- Surrogates generator using an initial sample in python
- Solution optimizer according to a surrogate in C++
- Solution evaluation using simulator in C++

## Configuration:

Requirements to run the project :
- Java (used version openjdk 11.0.20.1 2023-08-24)
- Javac (used version 11.0.20.1)
- Maven (used version 3.6.3)
- Osmosis (used version 0.48.3-1)
- Git (used version 3.6.3)
- cmake (used version 3.22.1)
- make (used version 4.3)
- gcc (used version 11.4.0)
- Python (used version 3.9.7)
- Python environment : we recommand to use a conda environment (used version 4.10.3), dependencies can be found in "src/simulator/eqasim-pipeline/environment.yml"
- Update the paths in "src/simulator/eqasim-pipeline/config_base.yml" by replacing "/path/to/project/directory" to the actual path of the project

Optionnal : 
- Taxicab for python if working on another bus line

## Missing Data

Some of the data files cannot be added to the git repository as they are too big.

Files to put in "bdtopo_hdf" :
- From https://geoservices.ign.fr/bdtopo, in the sidebar on the right, under "Téléchargement anciennes éditions", click on "BD TOPO® 2022 GeoPackage Départements" to go to the saved data publications from 2022 and download "BDTOPO\_3-0\_TOUSTHEMES\_GPKG\_LAMB93\_D059\_2022-03-15.7z" and "BDTOPO\_3-0\_TOUSTHEMES\_GPKG\_LAMB93\_D062\_2022-03-15.7z" by clicking on the download links for "Département 59 - Nord" and "Département 62 - Pas-de-Calais"

Files to put in "data/rp_2019" :
- From https://www.insee.fr/fr/statistiques/6544333 download "RP2019\_INDCVI\_csv.zip" by clicking on the csv download link under "Individus localisés au canton-ou-ville"
- From https://www.insee.fr/fr/statistiques/6543200 download "base-ic-evol-struct-pop-2019.zip" by clicking on the xlsx download link under "France hors Mayotte"
- From https://www.insee.fr/fr/statistiques/6456056 download "RP2019\_MOBPRO\_csv.zip" by clicking on the csv download link
- From https://www.insee.fr/fr/statistiques/6456052 download "RP2019\_MOBSCO\_csv.zip" by clicking on the csv download link

Files to put in "data/sirene" :
- From https://www.data.gouv.fr/fr/datasets/base-sirene-des-entreprises-et-de-leurs-etablissements-siren-siret/ download "StockEtablissement\_utf8.zip" and "StockUniteLegale\_utf8.zip" by clicking on the download links "Sirene : Fichier StockEtablissement du dd mmmmmm yyyy" and "Sirene : Fichier StockUniteLegale du dd mmmmmm yyyy"
- From https://www.data.gouv.fr/fr/datasets/geolocalisation-des-etablissements-du-repertoire-sirene-pour-les-etudes-statistiques/ download "GeolocalisationEtablissement\_Sirene\_pour\_etudes\_statistiques\_utf8.zip" by clicking on the download link "Sirene : Fichier GeolocalisationEtablissement_Sirene_pour_etudes_statistiques du dd mmmmmm yyyy"

## Other Data files sources
- Adreses database (folder "ban_hdf") : https://adresse.data.gouv.fr/data/ban/adresses/latest/csv/
- Service and facility census (folder "bpe_2021") : https://www.insee.fr/fr/statistiques/3568638
- Zoning registry (folder "codes_2021") : https://www.insee.fr/fr/information/2017499
- National household travel survey (folder "entd_2008") : https://www.statistiques.developpement-durable.gouv.fr/enquete-nationale-transports-et-deplacements-entd-2008
- Income Tax Data (folder "filosofi_2019") : https://www.insee.fr/fr/statistiques/6036907
- GTFS data (folder "gtfs_hdf") : https://transport.data.gouv.fr/datasets/horaires-theoriques-et-temps-reel-du-reseau-sitac-calais-gtfs-gtfs-rt (version might be different)
- IRIS data (folder "iris_2021") : https://geoservices.ign.fr/contoursiris
- OpenStreetMap data (folder "osm_hdf") : initial file from https://download.geofabrik.de/europe/france/nord-pas-de-calais.html and then manually processed

## To generate Surrogate Models:

Python generator generates surrogates models by computing the terms weights with regression on known samples using LassoLarsCV from sklearn.
In directory "python_generator":
```bash
python3 generator.py
```
Default parameters create 30 sparse surrogates models of order 2 and lag 2 learning on 1080 random solutions randomly picked from the file data/4000\_random\_solutions.txt.
Parameters can be changed in the command line:
```bash
usage: generator.py [-h] [-f FILENAME] [-e NBEVAL] [-n NBSOLS] [-o ORDER] [-s]
                    [-l LAG]

optional arguments:
  -h, --help            show this help message and exit
  -f FILENAME, --filename FILENAME
                        File of known solutions
  -e NBEVAL, --nbEval NBEVAL
                        Number of surrogates to create
  -n NBSOLS, --nbSols NBSOLS
                        Number of solutions to use in regression
  -o ORDER, --order ORDER
                        Order of walsh model to use
  -s, --notSparse       If not using a sparse model
  -l LAG, --lag LAG     Lag parameter of sparse surrogates  
```

## To compile:

```bash
mkdir build
cd build
cmake ..
make
```

## To generate solution according to a surrogate:

```bash
cd build
./src/exe/lsPortfolio -I=../instance/wmodel0.json -a=1 -s=1 --nghType=1 --nghSize=1.0 -k=0.1 -S=1 -t=2 -n=2 --out=out.dat --solutionOut=out.sol
```

Help for parameters:

```bash
cd build
./src/exe/lsPortfolio -h
```

## To evaluate a solution with the simulator

```bash
cd build/src/exe
./simple-matsim-iter `Solution` `Optional_folder_name`
```

Example: 

```bash
cd build/src/exe
./simple-matsim-iter 100000101010000100110101000111101100001101111101111001011011011110010111001111111011100000101110111010110110 sol_01
```

Warning: By using the folder_name parameter, new cache and output directories will be created which takes a lot of disk space. Use only when you want to keep a simulation output safe to further analyse it.

## Eqasim Main reference

https://github.com/eqasim-org/ile-de-france
> Hörl, S. and M. Balac (2021) [Synthetic population and travel demand for Paris and Île-de-France based on open and publicly available data](https://www.sciencedirect.com/science/article/pii/S0968090X21003016), _Transportation Research Part C_, **130**, 103291.
