#include <iostream>
#include <array>
#include <memory>
#include <chrono>
#include <utility>
#include <vector>
#include <algorithm>
#include <cmath>
#include <iterator>
#include <fstream>
#include <string>

static const std::string lyceeBusLine = "667730012";
static const std::string roseliereBusLine = "654110018";
static const double busSpeed = 5; // m/s

class Stop{
public:
    Stop(std::string pid, double plat, double plon){
        id = std::move(pid);
        lat = plat;
        lon = plon;
        order = -1;
        busLine = "none";
    }

    std::string id;
    double lat;
    double lon;
    int order;
    std::string busLine;

    bool operator==(const Stop & obj2) const
    {
        return this->id == obj2.id;
    }
};

void execCmd(const std::string& cmd, bool output = false, bool check = false){
    if(output){
        auto status = system(cmd.c_str());
        //std::cout << cmd << " command status : " << WIFEXITED(status) << " : " << status << std::endl;
    } else {
        auto status = system((cmd + " >/dev/null 2>/dev/null").c_str());
        //std::cout << cmd << " command status : " << WIFEXITED(status) << " : " << status << std::endl;
        if(WIFEXITED(status) == 1 && status == 256 && check) {
            std::cout << "Didn't work : " << WIFEXITED(status) << " : " << status << " ; Retrying once" << std::endl;
            status = system((cmd + " >/dev/null 2>/dev/null").c_str());
            if(WIFEXITED(status) == 1) {
                std::cout << "Something's wrong, shut down" << std::endl;
                exit(-1);
            }
        }
    }
}

double getPreciseDistanceBetweenCoordinates(double lat1, double lon1, double lat2, double lon2) {
    std::string cmd =
            "cd ../../../src/simulator/data/osm_hdf; python dist.py calais.osm " + std::to_string(lat1) + " " + std::to_string(lon1) + " " +
            std::to_string(lat2) + " " + std::to_string(lon2);
    std::array<char, 128> buffer{};
    std::string result;
    std::unique_ptr<FILE, decltype(&pclose)> pipe(popen(cmd.c_str(), "r"), pclose);
    if (!pipe) {
        throw std::runtime_error("popen() failed!");
    }
    while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr) {
        result += buffer.data();
    }
    return stod(result);
}

int timeToSecond(const std::string& time){
    std::string delimiter = ":";
    size_t pos_start = 0, pos_end, delim_len = delimiter.length();
    std::string token;
    std::vector<int> split;

    while ((pos_end = time.find (delimiter, pos_start)) != std::string::npos) {
        token = time.substr (pos_start, pos_end - pos_start);
        pos_start = pos_end + delim_len;
        split.push_back (stoi(token));
    }
    split.push_back (stoi(time.substr (pos_start)));

    return split[0] * 3600 + split[1] * 60 + split[2];
}

std::string secondToTime(int second){
    int hours = (int) second / 3600;
    int minutes = (int) (second % 3600) / 60;
    int seconds = (int) (second % 3600) % 60;
    std::string hoursStr = std::to_string(hours);
    if(hoursStr.size() < 2) hoursStr = "0" + hoursStr;
    std::string minutesStr = std::to_string(minutes);
    if(minutesStr.size() < 2) minutesStr = "0" + minutesStr;
    std::string secondsStr = std::to_string(seconds);
    if(secondsStr.size() < 2) secondsStr = "0" + secondsStr;
    return hoursStr + ":" + minutesStr + ":" + secondsStr;
}

class Simulator{
public:
    Simulator() {}

    Simulator(const std::string& prefix, int nbIters) : prefix(prefix), nbIters(nbIters) {
        execCmd("echo \" ---- Unzipping GTFS ----\";"
                "cd ../../../src/simulator/data/gtfs_hdf;"
                "unzip -o sitac-calais-rt-edit.zip_gtfs -d " + prefix);

        std::vector<std::pair<std::string, int>> lyceeBusStops = readStopsFromBusLine(lyceeBusLine);
        std::vector<std::pair<std::string, int>> roseliereBusStops = readStopsFromBusLine(roseliereBusLine);

        std::vector<Stop> stops;
        std::string delimiter = ",";
        std::ifstream infile("../../../src/simulator/data/gtfs_hdf/" + prefix + "/stops.txt");
        std::string line;
        size_t pos_start, pos_end, delim_len = delimiter.length();
        std::string token;
        std::vector<std::string> split;
        std::getline(infile, line);
        while (std::getline(infile, line)) {
            pos_start = 0;
            split.clear();
            while ((pos_end = line.find(delimiter, pos_start)) != std::string::npos) {
                token = line.substr(pos_start, pos_end - pos_start);
                pos_start = pos_end + delim_len;
                split.push_back(token);
            }

            Stop stop(split[0], stod(split[4]), stod(split[5]));

            auto it = std::find_if(lyceeBusStops.begin(), lyceeBusStops.end(),
                                   [&split](const std::pair<std::string, int> &element) {
                                       return element.first == split[0];
                                   });

            if (it != lyceeBusStops.end()) {
                stop.order = it->second;
                stop.busLine = lyceeBusLine;
            }

            it = std::find_if(roseliereBusStops.begin(), roseliereBusStops.end(),
                              [&split](const std::pair<std::string, int> &element) {
                                  return element.first == split[0];
                              });

            if (it != roseliereBusStops.end()) {
                stop.order = it->second;
                stop.busLine = roseliereBusLine;
            }

            stops.emplace_back(stop);
        }
        for(auto & stop : stops){
            sol.emplace_back(true, stop);
        }
        infile.close();
        readAllDistances();
        createCacheFolder();
        createOutputFolder();
        if(prefix != "base") {
            createConfigFile();
        }

        execCmd("echo \" ---- Zipping GTFS ----\";"
                "cd ../../../src/simulator/data/gtfs_hdf/" + prefix + ";"
                "zip sitac-calais-rt.zip ./*.txt");
    }

    std::vector<bool> getVector(){
        int lyceeMaxOrder = 0;
        int roseliereMaxOrder = 0;
        for(const auto& s : sol){
            if(s.second.busLine == lyceeBusLine){
                lyceeMaxOrder = std::max(lyceeMaxOrder, s.second.order);
            }
            if(s.second.busLine == roseliereBusLine){
                roseliereMaxOrder = std::max(roseliereMaxOrder, s.second.order);
            }
        }
        std::vector<bool> v;
        for(int i = 0; i <= lyceeMaxOrder; i++){
            bool found = false;
            int j = 0;
            while(!found){
                if(sol[j].second.busLine == lyceeBusLine && sol[j].second.order == i){
                    found = true;
                    v.emplace_back(sol[j].first);
                }
                j++;
            }
        }
        for(int i = 0; i <= roseliereMaxOrder; i++){
            bool found = false;
            int j = 0;
            while(!found){
                if(sol[j].second.busLine == roseliereBusLine && sol[j].second.order == i){
                    found = true;
                    v.emplace_back(sol[j].first);
                }
                j++;
            }
        }
        return v;
    }

    void setFromVector(std::vector<bool> v){
        int lyceeMaxOrder = 0;
        for(const auto& s : sol){
            if(s.second.busLine == lyceeBusLine){
                lyceeMaxOrder = std::max(lyceeMaxOrder, s.second.order);
            }
        }
        for(auto & s : sol){
            if(s.second.busLine == lyceeBusLine){
                s.first = v[s.second.order];
            }
            if(s.second.busLine == roseliereBusLine){
                s.first = v[s.second.order + lyceeMaxOrder + 1];
            }
        }
    }

    double getModeTraveltime(const std::string& m){
        std::ifstream infile("../../../src/simulator/output/" + prefix + "/npdc_restricted/simulation_output/eqasim_trips.csv");
        std::string line;
        std::vector<std::string> modes;
        std::vector<std::vector<double>> traveltimes;
        bool passHeader = true;
        while (std::getline(infile,line)) {
            if (passHeader) {
                passHeader = false;
                continue;
            }
            std::string delimiter = ";";
            std::string s = line;
            size_t pos;
            std::string token;
            std::vector<std::string> tokens;
            while ((pos = s.find(delimiter)) != std::string::npos) {
                token = s.substr(0, pos);
                tokens.emplace_back(token);
                s.erase(0, pos + delimiter.length());
            }
            tokens.emplace_back(s);

            std::string mode = tokens[10];
            double traveltime = std::stod(tokens[7]);
            auto it = find(modes.begin(), modes.end(), mode);
            if (it != modes.end())
            {
                int index = it - modes.begin();
                traveltimes[index].emplace_back(traveltime);
            }
            else {
                modes.emplace_back(mode);
                std::vector<double> v;
                v.emplace_back(traveltime);
                traveltimes.emplace_back(v);
            }
        }
        double avgTravelTime = 0;
        auto it = find(modes.begin(), modes.end(), m);
        if (it != modes.end())
        {
            int index = it - modes.begin();
            for(auto& traveltime : traveltimes[index]){
                avgTravelTime += traveltime;
            }
            avgTravelTime /= traveltimes[index].size();
        }
        infile.close();
        //std::cout << avgTravelTime << std::endl;
        return avgTravelTime;
    }

    void eval(){
        std::chrono::steady_clock::time_point t1 = std::chrono::steady_clock::now();
        std::time_t now = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
        setSolution();
        std::cout << "Running Pipeline at " << std::ctime(&now) << std::endl;
        execCmd("echo \" ---- Creating Population ----\";"
                "cd ../../../src/simulator/eqasim-pipeline/;"
                "python3 -m synpp config_" + prefix + ".yml", false, true);
        execCmd("echo \" ---- Cutting Scenario ----\";"
                "cd ../../../src/simulator/output/" + prefix + ";"
                "java -Xmx14G -cp npdc_run.jar org.eqasim.core.scenario.cutter.RunScenarioCutter --config-path npdc_config.xml --output-path npdc_restricted --extent-path ../../calais_restricted.shp --prefix npdc_restricted_ --threads 8", false, true);
        if(nbIters != 60) {
            setFileNbIters();
        }
        now = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
        std::cout << "Running Simulation at " << std::ctime(&now) << std::endl;
        execCmd("echo \" ---- Running Scenario ----\";"
                "cd ../../../src/simulator/output/" + prefix + "/npdc_restricted;"
                "java -Xmx14G -cp ../npdc_run.jar org.eqasim.ile_de_france.RunSimulation --config-path npdc_restricted_config.xml", false, true);
        std::chrono::steady_clock::time_point t2 = std::chrono::steady_clock::now();
        std::chrono::duration<double> time_span = std::chrono::duration_cast<std::chrono::duration<double> >(t2 - t1);
        std::cout.precision(std::numeric_limits< double >::max_digits10);
        std::cout << "Eval lasted " << time_span.count() << "s" << std::endl;
    }

    std::vector<std::pair<bool, Stop>>* getSolutionsStops(){
        return &sol;
    }

private:
    void createOutputFolder(){
        execCmd("cd ../../../src/simulator/output/;"
                "[ ! -d \"" + prefix + "\" ] && mkdir \"" + prefix + "\";"
                "cd " + prefix + ";"
                "rm -r ./*");
    }

    void createCacheFolder(){
        execCmd("cd ../../../src/simulator/cache/;"
                "[ ! -d \"" + prefix + "\" ] && mkdir \"" + prefix + "\"");
    }

    void createConfigFile(){
        execCmd("cd ../../../src/simulator/eqasim-pipeline/;"
                "[ ! -f \"config_" + prefix + ".yml\" ] && cp config_base.yml \"config_" + prefix + ".yml\"");
        std::ifstream infile("../../../src/simulator/eqasim-pipeline/config_" + prefix + ".yml");
        std::string contents;
        for (char ch; infile.get(ch); contents.push_back(ch)) {}
        infile.close();
        std::string search = "/base";
        std::string replace = "/" + prefix;
        auto pos = contents.find(search);
        while (pos != std::string::npos) {
            contents.replace(pos, search.length(), replace);
            pos = contents.find(search, pos);
        }
        std::ofstream outfile("../../../src/simulator/eqasim-pipeline/config_" + prefix + ".yml");
        outfile << contents;
        outfile.close();
    }

    std::vector<std::pair<std::string, int>> readStopsFromBusLine(const std::string& busLine){
        std::vector<std::pair<std::string, int>> stops;
        std::ifstream infile("../../../src/simulator/data/gtfs_hdf/" + prefix + "/stop_times.txt");
        std::string line;
        while (std::getline(infile, line)) {
            if (line.rfind(busLine, 0) == 0) {
                std::string delimiter = ",";
                std::string s = line;
                size_t pos;
                std::string token;
                std::vector<std::string> tokens;
                while ((pos = s.find(delimiter)) != std::string::npos) {
                    token = s.substr(0, pos);
                    tokens.emplace_back(token);
                    s.erase(0, pos + delimiter.length());
                }
                tokens.emplace_back(s);
                if (std::find_if(stops.begin(), stops.end(), [&tokens](const std::pair<std::string, int> &element) {
                    return element.first == tokens[3];
                }) == stops.end()) {
                    stops.emplace_back(tokens[3], stoi(tokens[4]));
                }
            }
        }
        infile.close();
        return stops;
    }

    void readAllDistances(){
        if(!std::ifstream("../../../src/simulator/data/osm_hdf/distances.csv")){
            //std::cout << "Writing all distances to new file" << std::endl;
            std::ofstream distances;
            distances.open("../../../src/simulator/data/osm_hdf/distances.csv");
            distances << "from,to,dist" << std::endl;
            for(auto & s1 : sol) {
                if (s1.second.busLine != "none") {
                    for (auto &s2: sol) {
                        if (s2.second.busLine == s1.second.busLine && s2.second.order > s1.second.order) {
                            double distance = getPreciseDistanceBetweenCoordinates(s1.second.lat, s1.second.lon,
                                                                                   s2.second.lat, s2.second.lon);
                            distances << s1.second.id << "," << s2.second.id << "," << std::to_string(distance)
                                      << std::endl;
                            knownDistances.push_back({{s1.second.id, s2.second.id}, distance});
                        }
                    }
                }
            }
            distances.close();
        }else{
            //std::cout << "Reading all distances from file" << std::endl;
            std::ifstream infile("../../../src/simulator/data/osm_hdf/distances.csv");
            std::string line;
            bool passHeader = true;
            while (std::getline(infile,line)) {
                if (passHeader) {
                    passHeader = false;
                    continue;
                }
                std::string delimiter = ",";
                std::string s = line;
                size_t pos;
                std::string token;
                std::vector<std::string> tokens;
                while ((pos = s.find(delimiter)) != std::string::npos) {
                    token = s.substr(0, pos);
                    tokens.emplace_back(token);
                    s.erase(0, pos + delimiter.length());
                }
                tokens.emplace_back(s);
                knownDistances.push_back({{tokens[0], tokens[1]}, stod(tokens[2])});
            }
        }
    }

    void writeStopTimes(){
        std::ifstream infile("../../../src/simulator/data/gtfs_hdf/" + prefix + "/stop_times.txt");
        std::string line;
        std::ofstream temp;
        std::string tempname = "temp" + prefix + ".txt";
        temp.open(tempname);
        bool passHeader = true;
        std::string lastTrip = "none";
        int lastId;
        double lastLat;
        double lastLon;
        int lastOrder;
        int lastDist;
        std::string lastTime;
        int nbline = 1;
        while (std::getline(infile,line)) {
            nbline++;
            if (passHeader) {
                temp << line << std::endl;
                passHeader = false;
                continue;
            }
            std::string delimiter = ",";
            std::string s = line;
            size_t pos;
            std::string token;
            std::vector<std::string> tokens;
            while ((pos = s.find(delimiter)) != std::string::npos) {
                token = s.substr(0, pos);
                tokens.emplace_back(token);
                s.erase(0, pos + delimiter.length());
            }
            tokens.emplace_back(s);

            int id = 0;
            bool notFound = true;
            while (notFound) {
                if (sol[id].second.id == tokens[3]) {
                    notFound = false;
                } else {
                    id++;
                }
            }

            if (sol[id].second.busLine != "none" &&
                (tokens[0].rfind(roseliereBusLine, 0) == 0 || tokens[0].rfind(lyceeBusLine, 0) == 0)) {
                if (sol[id].first) {
                    if (tokens[0] != lastTrip) {
                        lastTrip = tokens[0];
                        lastOrder = 0;
                        lastDist = 0;
                        lastTime = tokens[1];
                    } else {
                        lastOrder++;
                        double distance;
                        auto iter = std::find_if(knownDistances.begin(), knownDistances.end(), [&lastId, &id, this](
                                const std::pair<std::pair<std::string, std::string>, double> &element) {
                            return element.first.first == sol[lastId].second.id &&
                                   element.first.second == sol[id].second.id;
                        });
                        if (iter != knownDistances.end()) {
                            distance = iter->second;
                        } else {
                            //std::cout << "Distance between " << solution.sol[lastId].second.id << " and " << solution.sol[id].second.id << " is unknown" << std::endl;
                            distance = getPreciseDistanceBetweenCoordinates(lastLat, lastLon, sol[id].second.lat,
                                                                            sol[id].second.lon);
                            knownDistances.push_back(
                                    {{sol[lastId].second.id, sol[id].second.id}, distance});
                        }
                        lastDist += (int) distance;
                        int timeElapsed = (int) (distance / busSpeed);
                        lastTime = secondToTime(timeToSecond(lastTime) + timeElapsed);
                    }
                    lastId = id;
                    lastLat = sol[id].second.lat;
                    lastLon = sol[id].second.lon;
                    temp << lastTrip << "," << lastTime << "," << lastTime << "," << tokens[3] << "," << lastOrder << ",,,,"
                         << lastDist << "," << std::endl;
                }
            } else {
                temp << line << std::endl;
            }
        }
        temp.close();
        infile.close();
        std::string p = "../../../src/simulator/data/gtfs_hdf/" + prefix + "/stop_times.txt";
        remove(p.c_str());
        rename(tempname.c_str(),p.c_str());
    }

    void setSolution(){
        execCmd("echo \" ---- Unzipping GTFS ----\";"
                "cd ../../../src/simulator/data/gtfs_hdf;"
                "unzip -o sitac-calais-rt-edit.zip_gtfs -d " + prefix);

        writeStopTimes();

        execCmd("echo \" ---- Zipping GTFS ----\";"
                "cd ../../../src/simulator/data/gtfs_hdf/" + prefix + ";"
                "zip sitac-calais-rt.zip ./*.txt");
    }

    void setFileNbIters(){
        std::ifstream infile("../../../src/simulator/output/" + prefix + "/npdc_restricted/npdc_restricted_config.xml");
        std::string contents;
        for (char ch; infile.get(ch); contents.push_back(ch)) {}
        infile.close();
        std::string search = "\"60\"";
        std::string replace = "\"" + std::to_string(nbIters) + "\"";
        auto pos = contents.find(search);
        while (pos != std::string::npos) {
            contents.replace(pos, search.length(), replace);
            pos = contents.find(search, pos);
        }
        std::ofstream outfile("../../../src/simulator/output/" + prefix + "/npdc_restricted/npdc_restricted_config.xml");
        outfile << contents;
        outfile.close();
    }

    std::vector<std::pair<std::pair<std::string, std::string>, double>> knownDistances;
    std::vector<std::pair<bool, Stop>> sol;
    int nbIters;
    std::string prefix;
};
