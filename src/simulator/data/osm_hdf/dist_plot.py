import warnings
warnings.filterwarnings("ignore")
import osmnx as ox
import taxicab as tc
import os
import sys
import pickle

if not os.path.exists("graph.pkl"):
    G = ox.graph.graph_from_xml(sys.argv[1], simplify=True)
    filtr = ['tertiary', 'tertiary_link', 'secondary', 'unclassified']
    e = [(u, v, k) for u, v, k, d in G.edges(keys=True, data=True) if 'highway' not in d]
    G.remove_edges_from(e)
    G = ox.utils_graph.get_largest_component(G)
    pickle.dump(G, file=open("graph.pkl", "wb"))
else:
    G = pickle.load(open("graph.pkl", "rb"))

orig = (float(sys.argv[2]), float(sys.argv[3]))
dest = (float(sys.argv[4]), float(sys.argv[5]))
route = tc.shortest_path(G, orig, dest)
fig, ax = tc.plot_graph_route(G, route, route_color="c", node_size=0)
print(route[0])


#python dist.py '/media/valentin/Data/Linux_Files/EqasimOptim/data/osm/calais.osm' 50.953497 1.851323 50.950952 1.853868
#python dist.py '/media/valentin/Data/Linux_Files/EqasimOptim/data/osm/calais.osm' 50.940417 1.850982 50.959533 1.876274
#python dist.py '/media/valentin/Data/Linux_Files/EqasimOptim/data/osm/calais.osm' 50.94072 1.877061 50.941477 1.875475
#python dist.py '/media/valentin/Data/Linux_Files/EqasimOptim/data/osm/calais.osm' 50.953706 1.866082 50.955849 1.86449
