/*
<moWalshPerturb.h>

*/

#ifndef _moWalshPerturb_h
#define _moWalshPerturb_h

#include <eval/moEval.h>
#include <perturb/moPerturbation.h>
#include <memory/moDummyMemory.h>
#include <utils/eoRNG.h>
#include <moWalshIncrEval.h>
#include <moWalshDoubleIncrEval.h>

/**
 * Perturbation operator : based on k neighbors applications, with double incremental evaluation
 */
template< class Neighbor >
class moWalshPerturb : public moPerturbation<Neighbor>, public moDummyMemory<Neighbor> {

public:
    typedef typename Neighbor::EOT EOT;

    /**
     * Constructor
     * @param _eval neighbor evaluation (possibly incremental)
     * @param _size length of the bit string
     * @param _nbPertubation number of operator execution for perturbation
     */
    moWalshPerturb(eoRng & _rng,
        moWalshIncrEval<Neighbor>& _eval, moWalshDoubleIncrEval<Neighbor>& _deval, 
        unsigned int _nbPerturbation = 1) : rng(_rng), 
            eval(_eval), deval(_deval),  
            nbPerturbation(_nbPerturbation) 
    {
        size = eval.neighborsVector.size();
        index.resize(size);

        for(unsigned int i = 0; i < size; i++) {
            index[i] = i;
        }
    }

    /**
     * Apply exactly k moves in the solution, 
     * and used double incremental evaluation to compute the new fitness
     *
     * @param _solution to perturb
     * @return true
     */
    bool operator()(EOT& _solution) {
        Neighbor n;

        std::vector<unsigned int> bits;

        compute_bits(bits);

        for(unsigned int b : bits) {
            n.index(b);
            n.fitness(_solution.fitness() + _solution.neighborhoodContainer.fitness(b));

            deval(_solution, n);

            n.move(_solution);
            _solution.fitness(n.fitness());
        }

        return true;
    }

protected:
    unsigned int size;

    std::vector<unsigned int> index;

    virtual void compute_bits(std::vector<unsigned int> & _bits) {
        _bits.resize(nbPerturbation);

        unsigned int k;

        for(unsigned int i = 0; i < nbPerturbation; i++) {
            k = rng.uniform(size - i);

            _bits[i] = index[ k ];
            index[k] = index[size - i - 1];
            index[size - i - 1] = _bits[i];
        }
    }

private:
    eoRng & rng;

    moWalshIncrEval<Neighbor>& eval;

    // double incremental evaluation
    moWalshDoubleIncrEval<Neighbor> & deval;

    unsigned int nbPerturbation;


};

#endif
