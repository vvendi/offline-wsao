#ifndef __FirstImprovementNeighborSelect_h
#define __FirstImprovementNeighborSelect_h

/*

*/

#include <neighborSelect.h>
#include <moNeighborhoodContainer.h>
#include <utils/eoRNG.h>

template<class NghContainer>
class FirstImprovementNeighborSelect : public NeighborSelect<NghContainer> {
public:
	FirstImprovementNeighborSelect(eoRng & _rng) : rng(_rng) { }

	/*
		Select the neighbor given by the identifier
	*/
	virtual int operator()(NghContainer & _nghContainer) {
		return _nghContainer.improving(rng.uniform()); 
	}

private:
	eoRng & rng;
};


#endif