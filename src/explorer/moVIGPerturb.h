/*
<moVIGPerturb.h>

*/

#ifndef _moVIGPerturb_h
#define _moVIGPerturb_h

#include <eval/moEval.h>
#include <perturb/moPerturbation.h>
#include <memory/moDummyMemory.h>
#include <utils/eoRNG.h>
#include <moWalshIncrEval.h>
#include <moWalshDoubleIncrEval.h>

/**
 * VIG Perturbation operator :
 *   
 */
template< class Neighbor >
class moVIGPerturb : public moPerturbation<Neighbor>, public moDummyMemory<Neighbor> {

public:
    typedef typename Neighbor::EOT EOT;

    /**
     * Constructor
     * @param _eval neighbor evaluation (possibly incremental)
     * @param _size length of the bit string
     * @param _nbPertubation number of operator execution for perturbation
     */
    moVIGPerturb(eoRng & _rng,
        moWalshIncrEval<Neighbor>& _eval, moWalshDoubleIncrEval<Neighbor>& _deval, 
        unsigned int _nbPerturbation = 1) : rng(_rng), 
            eval(_eval), deval(_deval),  
            nbPerturbation(_nbPerturbation) 
    {
        size = eval.neighborsVector.size();
        index.resize(size);

        for(unsigned int i = 0; i < size; i++) {
            index[i] = i;
        }
    }

    /**
     * Apply exactly k moves in the solution, 
     * and used double incremental evaluation to compute the new fitness
     *
     * @param _solution to perturb
     * @return true
     */
    bool operator()(EOT& _solution) {
        Neighbor n;

        std::vector<unsigned int> bits;

        compute_bits(bits);

        for(unsigned int b : bits) {
            n.index(b);
            n.fitness(_solution.fitness() + _solution.neighborhoodContainer.fitness(b));

            deval(_solution, n);

            n.move(_solution);
            _solution.fitness(n.fitness());
        }

        return true;
    }

protected:
    unsigned int size;

    std::vector<unsigned int> index;

    virtual void compute_bits(std::vector<unsigned int> & _bits) {
        _bits.resize(nbPerturbation);

        // selected bit
        unsigned iselect = rng.uniform(size);

        if (eval.interactions[iselect].size() > 1) { // self-interaction in eval.interactions
            if (eval.interactions[iselect].size() <= nbPerturbation) {
                unsigned k = 0;
                for(auto it = eval.interactions[iselect].begin(); it != eval.interactions[iselect].end(); ++it) {
                    _bits[k] = it->first;
                    k++;
                }
            } else {
                unsigned k = 0;
                std::vector<unsigned int> ind(eval.interactions[iselect].size() - 1);
                for(auto it = eval.interactions[iselect].begin(); it != eval.interactions[iselect].end(); ++it) {
                    if (it->first != iselect) {
                        ind[k] = it->first;
                        k++;                        
                    }
                }

                _bits[0] = iselect;
                for(unsigned int i = 0; i < nbPerturbation - 1; i++) {
                    k = rng.uniform(ind.size() - i);

                    _bits[i + 1] = ind[ k ];
                    ind[k] = ind[ind.size() - i - 1];
                    ind[ind.size() - i - 1] = _bits[i + 1];
                }
            }
        } else {
            _bits[0] = iselect;

            unsigned int k;

            for(unsigned int i = 0; i < nbPerturbation - 1; i++) {
                k = rng.uniform(size - i);
                while (index[ k ] == iselect)
                    k = rng.uniform(size - i);

                _bits[i + 1] = index[ k ];
                index[k] = index[size - i - 1];
                index[size - i - 1] = _bits[i + 1];
            }
        }
    }

private:
    eoRng & rng;

    moWalshIncrEval<Neighbor>& eval;

    // double incremental evaluation
    moWalshDoubleIncrEval<Neighbor> & deval;

    unsigned int nbPerturbation;


};

#endif
