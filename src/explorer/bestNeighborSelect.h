#ifndef __BestNeighborSelect_h
#define __BestNeighborSelect_h

/*

*/

#include <neighborSelect.h>
#include <moNeighborhoodContainer.h>

template<class NghContainer>
class BestNeighborSelect : public NeighborSelect<NghContainer> {
public:

	/*
		Select the neighbor given by the identifier

		@param _nghContainer vector of fitness value difference
		@return identifier -1: no selection
	*/
	virtual int operator()(NghContainer & _nghContainer) {
		return _nghContainer.maximum();
	}

};


#endif