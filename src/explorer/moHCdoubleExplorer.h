/*
*/

#ifndef _moHCdoubleExplorer_h
#define _moHCdoubleExplorer_h

#include <explorer/moNeighborhoodExplorer.h>
#include <comparator/moNeighborComparator.h>
#include <comparator/moSolNeighborComparator.h>
#include <neighborhood/moNeighborhood.h>
#include <neighborhood/moOrderNeighborhood.h>
#include <bst.h>
#include <moWalshIncrEval.h>
#include <moWalshDoubleIncrEval.h>
#include <neighborSelect.h>
#include <moNeighborhoodContainer.h>
#include <moEfficientNeighborhoodContainer.h>

/**
 * Explorer for a Hill-climbing using Double incremental evaluation
 */
template< class Neighbor >
class moHCdoubleExplorer : public moNeighborhoodExplorer<Neighbor>
{
public:
    typedef typename Neighbor::EOT EOT ;
    typedef moNeighborhood<Neighbor> Neighborhood ;
    typedef typename Neighbor::EOT::Fitness Fitness ;

    using moNeighborhoodExplorer<Neighbor>::neighborhood;
    using moNeighborhoodExplorer<Neighbor>::eval;
    using moNeighborhoodExplorer<Neighbor>::currentNeighbor;
    using moNeighborhoodExplorer<Neighbor>::selectedNeighbor;
    using moNeighborhoodExplorer<Neighbor>::moveApplied;

    /**
     * Constructor
     * @param _neighborhood the neighborhood
     * @param _eval the evaluation function
     * @param _neighborComparator a neighbor comparator
     * @param _solNeighborComparator solution vs neighbor comparator
     */
    moHCdoubleExplorer(Neighborhood& _neighborhood, moWalshIncrEval<Neighbor>& _eval, moWalshDoubleIncrEval<Neighbor>& _deval, /* moEval */
                       NeighborSelect<typename Neighbor::EOT::NghContainer> & _neighborSelect,
                       moComparator<EOT, Neighbor>& _solNeighborComparator) : 
//                       moSolNeighborComparator<Neighbor>& _solNeighborComparator) : 
        moNeighborhoodExplorer<Neighbor>(_neighborhood, _eval), 
        deval(_deval),
        neighborSelect(_neighborSelect),
        solNeighborComparator(_solNeighborComparator) {
        isAccept = false;
    }

    /**
     * Destructor
     */
    ~moHCdoubleExplorer() {
    }

    /**
     * initParam: 
     * @param _solution unused solution
     */
    virtual void initParam(EOT & _solution) {
        // compute all delta (vector) for the first time usign incremental evaluation

        if (neighborhood.hasNeighbor(_solution) && _solution.neighborhoodContainer.invalid()) { 
            _solution.neighborhoodContainer.init(deval.size()); 

            // evaluate all neighbors using incremental evaluation if necessary
            deval(_solution);
        }
    };

    /**
     * updateParam: NOTHING TO DO
     * @param _solution unused solution
     */
    virtual void updateParam(EOT & _solution) {
    }

    /**
     * terminate: NOTHING TO DO
     * @param _solution unused solution
     */
    virtual void terminate(EOT & _solution) {};

    /**
     * Explore the neighborhood of a solution
     * @param _solution the current solution
     */
    virtual void operator()(EOT & _solution) {
        // if _solution has a Neighbor
        if (neighborhood.hasNeighbor(_solution)) {
            // Select an id 
            int imove;
            double selectedFitness;

            // selection of one neighbor (worst improvemnt, best improvement, first, etc.)
            imove = neighborSelect(_solution.neighborhoodContainer);

            if (imove >= 0) {
                // Update selectedNeighbor
                selectedNeighbor.index(imove);
                selectedNeighbor.fitness(_solution.fitness() + _solution.neighborhoodContainer.fitness(imove));
                
                // Update delta according to selectedNeighbors, before the move
                if (solNeighborComparator(_solution, selectedNeighbor)) {
                    // Update deltas according to imove
                    deval(_solution, selectedNeighbor);
                    isAccept = true;
                } else
                    isAccept = false;
            } else {
              isAccept = false;
            }
        } else {
          // if _solution hasn't neighbor,
          isAccept = false;
        }
    };

    /**
     * continue if a move is accepted
     * @param _solution the solution
     * @return true if an ameliorated neighbor was be found
     */
    virtual bool isContinue(EOT & _solution) {
        return isAccept ;
    };

    /**
     * accept test if an amelirated neighbor was be found
     * @param _solution the solution
     * @return true if the best neighbor ameliorate the fitness
     */
    virtual bool accept(EOT & _solution) {
        if (neighborhood.hasNeighbor(_solution)) { 
            isAccept = isAccept and solNeighborComparator(_solution, selectedNeighbor) ;
        }
        return isAccept;
    };

    /**
     * Return the class Name
     * @return the class name as a std::string
     */
    virtual std::string className() const {
        return "moHCdoubleExplorer";
    }

private:
    // double incremental evaluation
    moWalshDoubleIncrEval<Neighbor> & deval;

    // selection strategy of a neighbor
    NeighborSelect<typename Neighbor::EOT::NghContainer> & neighborSelect;

    // comparator betwenn solution and neighbor or between neighbors
    //moSolNeighborComparator<Neighbor>& solNeighborComparator;
    moComparator<EOT, Neighbor>& solNeighborComparator;

    // true if the move is accepted
    bool isAccept ;

};


#endif
