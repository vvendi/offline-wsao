#ifndef __NeighborSelect_h
#define __NeighborSelect_h

/*
	Select a neighborhood, from the neighborhood
	Efficient selection using Binary Search Tree (BST)
*/

//#include <bst.h>
#include <moWalshIncrEval.h>
#include <moNeighborhoodContainer.h>

template<class NghContainer>
class NeighborSelect {
public:

	/*
		Select the neighbor given by the identifier

		@param _nghContainer vector of fitness value difference
		@return identifier -1: no selection
	*/
	virtual int operator()(NghContainer & _nghContainer) = 0;

};


#endif