/*
  <moDRILSexplorer.h>

  from:  <moILSexplorer.h>

  Author: SV - 2021/02/25
*/

#ifndef _moDRILSexplorer_h
#define _moDRILSexplorer_h

#include <explorer/moNeighborhoodExplorer.h>
#include <comparator/moNeighborComparator.h>
#include <comparator/moSolNeighborComparator.h>
#include <algo/moLocalSearch.h>
#include <perturb/moPerturbation.h>
#include <acceptCrit/moAcceptanceCriterion.h>


/**
 * Explorer for an Iterated Local Search
 */


template< class Neighbor, class NeighborLO >
class moDRILSexplorer : public moNeighborhoodExplorer< NeighborLO >
{
public:
  typedef moNeighborhood<Neighbor> Neighborhood ;
  typedef typename Neighbor::EOT EOT;
  typedef moNeighborhood<NeighborLO> NeighborhoodLO;
  

    /**
     * Constructor
     * @param _ls a local search
     * @param _perturb a perturbation operator
     * @param _acceptCrit a acceptance criteria
     */
    moDRILSexplorer(moLocalSearch<Neighbor>& _ls, moPerturbation<Neighbor>& _perturb, 
                    eoQuadOp<EOT> & _xover, moAcceptanceCriterion<Neighbor>& _acceptCrit) : moNeighborhoodExplorer<NeighborLO>(), 
                ls(_ls), perturb(_perturb), xover(_xover), acceptCrit(_acceptCrit) {
        firstIteration=true;
    }

    /**
     * Destructor
     */
    ~moDRILSexplorer() {

    }

    /**
     * Init perturbation and acceptance criteria
     * @param _solution the current solution
     */
    virtual void initParam(EOT & _solution) {
        firstIteration=true;
        perturb.init(_solution);
        acceptCrit.init(_solution);
    };

    /**
     * Update perturbation and acceptance criteria
     * @param _solution the current solution
     */
    virtual void updateParam(EOT & _solution) {
        if ((*this).moveApplied()) {
            perturb.add(_solution,emptyNeighbor);
            acceptCrit.add(_solution,emptyNeighbor);
        }
        perturb.update(_solution, emptyNeighbor);
        acceptCrit.update(_solution, emptyNeighbor);
    };

    /**
     * terminate: NOTHING TO DO
     * @param _solution a solution (unused)
     */
    virtual void terminate(EOT & _solution) {};

    /**
     * Perturb and apply local search on a solution
     * @param _solution the solution
     */
    virtual void operator()(EOT & _solution) {
        //copy the solution to perform new local search
        currentSol = _solution;

        //perturb solution exept at the first iteration
        if (!firstIteration) {
            perturb(currentSol);
        }

        //apply the local search on the copy
        ls(currentSol);

        if (!firstIteration) {
            xover(_solution, currentSol);  // only the second solution is modified (i.e. currentSol)

            if (acceptCrit(_solution, currentSol)) { // if improvement
                ls(currentSol);
            }          
        } else
            firstIteration = false;

    };

    /**
     * Always continue
     * @param _solution the solution
     * @return always true
     */
    virtual bool isContinue(EOT & _solution) {
        return true;
    };

    /**
     * copy the solution found by the local search
     * @param _solution the solution
     */
    virtual void move(EOT & _solution) {
        _solution=currentSol;
    };

    /**
     * accept test if an ameliorated neighbor was found
     * @param _solution the solution
     * @return true if acceptance criteria is verified
     */
    virtual bool accept(EOT & _solution) {
        return acceptCrit(_solution, currentSol);
    };

    /**
     * Return the class Name
     * @return the class name as a std::string
     */
    virtual std::string className() const {
        return "moDRILSexplorer";
    }

private:
    //Usefull to use the memory of tabuSearch
    Neighbor emptyNeighbor;
    EOT currentSol;
    moLocalSearch<Neighbor>& ls;
    moPerturbation<Neighbor> & perturb;
    eoQuadOp<EOT> & xover;
    moAcceptanceCriterion<Neighbor>& acceptCrit;

    bool firstIteration;
};


#endif
