#ifndef __BST_h
#define __BST_h

/*
  Binary search Tree : AVL
*/

#include <iostream>
#include <math.h>
#include <vector>

// precision, equality between two numbers
#define EPSILON 1e-10

template<typename Fitness>
class Node {
public:
    Fitness fitness;
    unsigned neighbor;

    int height;
    Node * left;
    Node * right;
    unsigned int size; // number of elements in the tree

    Node(Fitness _fitness, unsigned _neighbor) : fitness(_fitness), neighbor(_neighbor), height(1), left(NULL), right(NULL), size(1) {}

    Node(Node<Fitness> * _source) : fitness(_source->fitness), neighbor(_source->neighbor), height(1), left(NULL), right(NULL), size(1) {}

    Node(const Node<Fitness> & _source) : fitness(_source.fitness), neighbor(_source.neighbor), height(_source.height), left(_source.left), right(_source.right), size(_source.size) {}

    ~Node() {
    }

    Node& operator=(const Node & _source) {
        fitness  = _source.fitness;
        neighbor = _source.neighbor; 
        height   = _source.height; 
        left     = _source.left; 
        right    = _source.right;
        size     = _source.size;

        return *this;
    }

    void updateHeight() {
        if (left == NULL) {
            if (right == NULL) {
                height = 1;
                size   = 1;
            }
            else {
                height = right->height + 1;
                size   = right->size + 1;
            }
        } else
            if (right == NULL) {
                height = left->height + 1;
                size   = left->size + 1;
            }
            else {
                height = std::max(left->height, right->height) + 1;
                size   = left->size + right->size + 1;
            }
    }

    int balanceIndex() {
        if (left == NULL) {
            if (right == NULL)
                return 0;
            else
                return - right->height;
        } else
            if (right == NULL)
                return left->height ;
            else
                return left->height - right->height;
    }

    void print(std::ostream & _out) const {
        _out << "{" << size << " " << height << ", " << fitness << " " << neighbor << "}";
    }
};


template<typename Fitness>
class BST {
public:
    BST() : root(NULL) { }

    ~BST() {
        destroy(root);
    }

    BST(const BST & _bst) { 
        if (_bst.root == NULL) 
            root = NULL;
        else
            root = copy(_bst.root);
    }

    BST& operator=(const BST & _bst) { 
        if (_bst.root == NULL) 
            root = NULL;
        else
            root = copy(_bst.root);

        return *this;
    }

    void makeEmpty() {
        destroy(root);
        root = NULL;
    }

    bool empty() const {
        return root == NULL;
    }

    unsigned int size() const {
        if (root == NULL) 
            return 0;
        else
            return root->size;
    }

    void insert(Fitness _fitness, unsigned _neighbor) {
        root = insert(root, _fitness, _neighbor);
    }

    void remove(Fitness _fitness, unsigned & _neighbor) {
        root = remove(root, _fitness, _neighbor);
    }

    void minimum(Fitness & vmin, int & min) {
        if (root == NULL) { // empty
            min = -1;
        } else {
            Node<Fitness> * n = minimum(root);

            min  = n->neighbor;
            vmin = n->fitness;            
        }
    }   

    void maximum(Fitness & vmax, int & max) {
        if (root == NULL) { // empty
            max = -1;
        } else {
            Node<Fitness> * n = maximum(root);

            max  = n->neighbor;
            vmax = n->fitness;
        }
    }

    /*
        Return a random element according to the random number rnd
    */
    void random(double rnd, Fitness & v, int & id) {
        if (root == NULL)
            id = -1;
        else {
            if (rnd >= 0 && rnd < 1) {
                Node<Fitness> * n = random(rnd * root->size, root);

                id = n->neighbor;
                v  = n->fitness;            
            } else {
                id = -1;
            }
        }
    }

    void upperThan(Fitness v, Fitness & vmax, int & max) {
        if (root == NULL) { // empty
            max = -1;
        } else {
            Node<Fitness> * n = upperThan(v, root);

            if (n == NULL) 
                n = maximum(root); // no neighbor greater than v, so the maximum is returned

            max  = n->neighbor;
            vmax = n->fitness;
        }
    }

    void allmax(Fitness & _fitness, unsigned & _nb, std::vector< unsigned > & _res) {
        _nb = 0;

        if (root != NULL) {
            _fitness = root->fitness;
            allmax(root, _fitness, _nb, _res);            
        }
    }

    void findall(Fitness _fitness, unsigned & _nb, std::vector< unsigned > & _res) {
        _nb = 0;
        findall(root, _fitness, _nb, _res);
    }

    void print(std::ostream & _out) const {
        prefixe(root, _out);
    }

    void copy(BST<Fitness> & _dest) {
        _dest.root = copy(root);
    }

    void copy(Node<Fitness> * _source, BST<Fitness> & _dest) {
        _dest.root = copy(_source);
    }

    bool checkHeight() {
        if (root == NULL)
            return true;
        else
            return (root->height == height(root));
    }

    bool checkBalance() {
        if (root == NULL)
            return true;
        else
            return checkBalance(root);
    }

    Node<Fitness> * root;

private:
    void destroy(Node<Fitness> * n) {
        if (n != NULL) {
            destroy(n->left);
            destroy(n->right);
            delete n;
        }
    }

    Node<Fitness>* copy(Node<Fitness> * n) {
        if (n == NULL) 
            return n;
        else {
            Node<Fitness> * cn = new Node<Fitness>(n);
            cn->left   = copy(n->left);
            cn->right  = copy(n->right);
            cn->height = n->height;
            cn->size   = n->size;

            return cn;
        }
    }

    unsigned height(Node<Fitness> * n) {
        if (n == NULL)
            return 0;
        else {
            unsigned l = height(n->left);
            unsigned r = height(n->right);
            return std::max(l, r) + 1;
        }
    }

    bool checkBalance(Node<Fitness> * n) {
        if (n == NULL)
            return true;
        else {
            bool l = checkBalance(n->left);
            bool r = checkBalance(n->right);
            return l && r && (std::abs(n->balanceIndex()) < 2);
        }
    }

    Node<Fitness> * minimum(Node<Fitness> * n) {
        if (n == NULL)
            return NULL;
        else {
            if (n->left == NULL)
                return n;
            else
                return minimum(n->left);
        }
    }

    Node<Fitness> * maximum(Node<Fitness> * n) {
        if (n == NULL)
            return NULL;
        else {
            if (n->right == NULL)
                return n;
            else
                return maximum(n->right);
        }
    }

    Node<Fitness> * random(double rndValue, Node<Fitness> * n) {
        if (n == NULL)
            return NULL;
        else {
            unsigned int lsize ;

            if (n->left == NULL) 
                lsize = 0;
            else 
                lsize = n->left->size;

            if (rndValue < lsize)
                return random(rndValue, n->left);
            else
                if (rndValue < (lsize + 1))
                    return n;
                else
                    return random(rndValue - (lsize + 1), n->right);
        }
    }

    Node<Fitness> * upperThan(Fitness v, Node<Fitness> * n) {
        if (n == NULL)
            return NULL;
        else {
            if (n->fitness < v)
                return upperThan(v, n->right);
            else {
                Node<Fitness> * res = upperThan(v, n->left);

                if (res == NULL)
                    return n;
                else
                    return res;                    
            }
        }
    }

    void allmax(Node<Fitness> * n, Fitness & fit, unsigned & nb, std::vector< unsigned > & res) {
        if (n != NULL) {
            //if (fit == n->fitness) {
            if (abs(fit - n->fitness) < EPSILON) {  // EPSILON precision on numbers
                res[nb] = n->neighbor;
                nb++;
                allmax(n->right, fit, nb, res);
            } else 
                if (fit < n->fitness) {
                    fit = n->fitness;
                    res[0] = n->neighbor;
                    nb = 1;
                    allmax(n->right, fit, nb, res);                    
                }
        }
    }

    void findall(Node<Fitness> * n, Fitness fit, unsigned & nb, std::vector< unsigned > & res) {
        if (n != NULL) {
            if (fit == n->fitness) {
                res[nb] = n->neighbor;
                nb++;
                findall(n->left, fit, nb, res);
                findall(n->right, fit, nb, res);                
            } else 
                if (fit < n->fitness) 
                    findall(n->left, fit, nb, res);
                else
                    findall(n->right, fit, nb, res);                
        }
    }

    Node<Fitness>* insert(Node<Fitness> * n, Fitness _fitness, unsigned _neighbor) {
        if (n == NULL) {
            n = new Node<Fitness>(_fitness, _neighbor);
        } else {
            if (_fitness < n->fitness || 
                (_fitness == n->fitness && _neighbor < n->neighbor)) {
                n->left = insert(n->left, _fitness, _neighbor);
                n->updateHeight();
                n = balance(n);
            } else {
                n->right = insert(n->right, _fitness, _neighbor);
                n->updateHeight();
                n = balance(n);
            }
        }
        return n;
    }

    Node<Fitness>* remove(Node<Fitness> * n, Fitness _fitness, unsigned _neighbor) {
        if (n != NULL) {
            if (n->fitness == _fitness && n->neighbor == _neighbor) {
                // remove this node
                if (n->left == NULL) {
                    Node<Fitness> * tmp = n;

                    n = balance(n->right);
                    //n = balance(n);
                    delete tmp;
                } else {
                    std::pair<Node<Fitness>*, Node<Fitness>*> m = cutMax(n->left);
                    m.second->left  = m.first;

                    m.second->right = n->right;

                    m.second->updateHeight();
                    delete n;
                    n = m.second;
                }
            } else 
                if (_fitness < n->fitness || 
                    (_fitness == n->fitness && _neighbor < n->neighbor)) {
                        n->left = remove(n->left, _fitness, _neighbor);
                        n->updateHeight();
                    } else {
                        n->right = remove(n->right, _fitness, _neighbor);
                        n->updateHeight();
                    }

            n = balance(n);
        }

        return n;
    }

    std::pair<Node<Fitness>*, Node<Fitness>*> cutMax(Node<Fitness> * n) {
        if (n->right == NULL) {
            return std::pair<Node<Fitness>*, Node<Fitness>*>(n->left, n);
        } else {
            std::pair<Node<Fitness>*, Node<Fitness>*> res = cutMax(n->right);

            n->right = res.first;

            n->updateHeight();
            
            n = balance(n);

            return std::pair<Node<Fitness>*, Node<Fitness>*>(n, res.second);            
        }
    }

    void prefixe(Node<Fitness> * n, std::ostream & _out) const {
        _out << "(";
        if (n != NULL) {
            n->print(_out);
            prefixe(n->left, _out);
            prefixe(n->right, _out);
        }
        _out << ")";
    }

    int heightNode(Node<Fitness> * n) {
        return n ? n->height : 0;
    }

    Node<Fitness>* balance(Node<Fitness> * n) {
        if (n != NULL) {
            int b = n->balanceIndex();
            if (std::abs(b) > 1) {
                if (b == 2) {
                    if (heightNode(n->left->left) >= heightNode(n->left->right)) {
                        n = rotateRight(n);
                    } else {
                        n->left = rotateLeft(n->left);
                        n = rotateRight(n);
                    }
                } else {
                    if (heightNode(n->right->right) >= heightNode(n->right->left)) {
                        n = rotateLeft(n);
                    } else {
                        n->right = rotateRight(n->right);
                        n = rotateLeft(n);
                    }
                }
            }            
        }

        return n;
    }

    Node<Fitness>* rotateLeft(Node<Fitness> * p) {
        // right 
        Node<Fitness> * rt = p->right;

        // left of the right
        p->right = rt->left;
        p->updateHeight();

        rt->left = p;
        rt->updateHeight();

        return rt;
    }

    Node<Fitness>* rotateRight(Node<Fitness> * p) {
        // left
        Node<Fitness> * lt = p->left;

        p->left = lt->right;
        p->updateHeight();

        lt->right = p;
        lt->updateHeight();

        return lt;
    }
};

#endif