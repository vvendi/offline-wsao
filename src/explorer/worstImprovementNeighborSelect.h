#ifndef __WorstImprovementNeighborSelect_h
#define __WorstImprovementNeighborSelect_h

/*

*/

#include <neighborSelect.h>
#include <moNeighborhoodContainer.h>

template<class NghContainer>
class WorstImprovementNeighborSelect : public NeighborSelect<NghContainer> {
public:

	/*
		Select the neighbor given by the identifier
	*/
	virtual int operator()(NghContainer & _nghContainer) {
		return _nghContainer.worstImproving(); 
	}

};


#endif