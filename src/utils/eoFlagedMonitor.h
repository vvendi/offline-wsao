//-----------------------------------------------------------------------------
// from eoTimedMonitor.h

//-----------------------------------------------------------------------------

#ifndef _eoFlagedMonitor_h
#define _eoFlagedMonitor_h

#include <string>

#include <utils/eoMonitor.h>
#include <eoObject.h>

/**
    Holds a collection of monitors and only fires them when a flag is true

*/
class eoFlagedMonitor : public eoMonitor
{
public:

    /** Constructor

    @param seconds_ Specify time limit (s).
    */
    eoFlagedMonitor(eoValueParam<bool> & _flag) : flag(_flag) {}

    eoMonitor& operator()(void) {

        if (flag.value()) {
            for (unsigned i = 0; i < monitors.size(); ++i) {
                (*monitors[i])();
            }
        }

        return *this;
    }

    void add(eoMonitor& mon) { monitors.push_back(&mon); }

  virtual std::string className(void) const { return "eoFlagedMonitor"; }

private:
    eoValueParam<bool> & flag;

    std::vector<eoMonitor*> monitors;
};

#endif
