/*
  <moImprovementStat.h>
*/

#ifndef moImprovementStat_h
#define moImprovementStat_h

#include <continuator/moStat.h>

/**
 * The statistic which save the best solution found during the search
 */
template <class EOT>
class moImprovementStat : public moStat<EOT, bool>
{
public :
    using moStat<EOT, bool>::value;

    /**
     * Default Constructor
     * @param _reInitSol when true the best so far is reinitialized
     */
    moImprovementStat(): moStat<EOT, bool>(true, "improvement"), firstTime(true) {
    }

    /**
     * Initialization of the best solution on the first one
     * @param _sol the first solution
     */
    virtual void init(EOT & _sol) {
        if (firstTime) {
            value() = true;
            fitness = _sol.fitness();
            firstTime = false;
        }
    }

    virtual void reset() {
        firstTime = true;
    }

    /**
     * Update the best solution
     * @param _sol the current solution
     */
    virtual void operator()(EOT & _sol) {
        if (fitness < _sol.fitness())
        {
            fitness = _sol.fitness();
            value() = true;
        } else {
            value() = false;
        }
    }

    /**
     * @return name of the class
     */
    virtual std::string className(void) const {
        return "moImprovementStat";
    }


protected:
  typename EOT::Fitness fitness;
  bool firstTime;

};

#endif
