#ifndef __partition_xover__h
#define __partition_xover__h

#include <eoOp.h>
#include <moWalshIncrEval.h>

template<class Neighbor> 
class eoPartitionXover: public eoQuadOp<typename Neighbor::EOT> {
public:
	typedef typename Neighbor::EOT EOT;

	eoPartitionXover(moWalshIncrEval<Neighbor> & _incrEval) : eval(_incrEval) {
		nComponents = 0;

		component_id.resize(eval.walshEval.n);

		components.resize(eval.walshEval.n + 1);

		partial_func_terms.resize(eval.walshEval.n + 1);
	}


	bool operator()(EOT & x1, EOT & x2) {
		EOT child;
		child.resize(x1.size());
		child.parity.resize(x1.parity.size());

		compute_connected_components(x1, x2);

		compute_partial_function_terms();

		double g1, g2;

		// true when at least one component is used from xi
		bool n1 = false;
		bool n2 = false;

		// first common component : copy x1 into child
		double child_fit = partial_func(0, x1) ;
		
		for(unsigned i : components[0])
			child[i] = x1[i];
		for(unsigned k : partial_func_terms[0])
			child.parity[k] = x1.parity[k];

		// Others true components
		for(unsigned c = 1; c <= nComponents; c++) {
			g1 = partial_func(c, x1);
			g2 = partial_func(c, x2);

			if (g1 <= g2) {
				// x2 is better for that component
				n2 = true;
				child_fit += g2;

				for(unsigned i : components[c])
					child[i] = x2[i];
				for(unsigned k : partial_func_terms[c])
					child.parity[k] = x2.parity[k];
			} else {
				// x1 is better for that component
				n1 = true;
				child_fit += g1;				

				for(unsigned i : components[c])
					child[i] = x1[i];
				for(unsigned k : partial_func_terms[c])
					child.parity[k] = x1.parity[k];
			}
		}

		child.fitness(child_fit);

		if (n1 && n2) {
			child.neighborhoodContainer.invalidate();
			x2 = child; // I'd like to avoid that copy...

			return true;
		} else
			return false;

		//return (n1 && n2); // true when child is different from x1 and x2
	}

	void print_state() {
		std::cout << "ids:" ;
		for(unsigned i : component_id) {
			std::cout << " "  << i ;
		}
		std::cout << std::endl;

		std::cout << nComponents << std::endl;

		for(unsigned c = 0; c <= nComponents; c++) {
			std::cout << c << ":" ;
			for(unsigned i : components[c])
				std::cout << " " << i ;
			std::cout << std::endl;
		}

		std::cout << std::endl;

		for(unsigned c = 0; c <= nComponents; c++) {
			std::cout << c << ":" ;
			for(unsigned i : partial_func_terms[c])
				std::cout << " " << i ;
			std::cout << std::endl;
		}
	}

	// list of variables of each connected component
	std::vector< std::vector<unsigned> > components;

	// number of connected components
	unsigned nComponents;

	// id of the connected component of each variable (vector of size n)
	std::vector<int> component_id;

	// terms of the Walsh function of each component
	std::vector< std::vector<unsigned> > partial_func_terms;

protected:
	moWalshIncrEval<Neighbor> & eval;

	void compute_connected_components(EOT & x1, EOT & x2) {
		nComponents = 0;

		std::fill(component_id.begin(), component_id.end(), -1);

		// share bits in the component 0
		for(unsigned i = 0; i < eval.walshEval.n; i++) // avoid to copy the references on x1 and x2 in set_component function
			if (x1[i] == x2[i])
				component_id[i] = 0; // in the share variable component

		// for all bits
		for(unsigned i = 0; i < eval.walshEval.n; i++) {
			if (component_id[i] == -1) {
				nComponents++;

				set_component(i);
			}
		}

		// bits in each component
		for(unsigned c = 0; c <= nComponents; c++) 
			components[c].resize( 0 );

		for(unsigned i = 0; i < eval.walshEval.n; i++) 
			components[ component_id[i] ].push_back(i);
	}

	void set_component(unsigned i) {
		if (component_id[i] == -1) {
			component_id[i] = nComponents;

			for(auto & j : eval.interactions[i]) // all neighbors in the variable interaction graph
				set_component(j.first);
		}
	}

	void compute_partial_function_terms() {
		unsigned i;

		for(unsigned c = 0; c <= nComponents; c++) 
			partial_func_terms[c].resize( 0 );


		for(unsigned k = 0; k < eval.walshEval.p; k++) {
			std::vector<unsigned> & coef = eval.walshEval.coefs[k];

			i = 0;
			while (i < coef.size() && component_id[ coef[i] ] == 0) i++;

			if (i == coef.size())
				partial_func_terms[0].push_back( k );
			else
				partial_func_terms[ component_id[ coef[i] ] ].push_back( k );
		}
	}

	double partial_func(unsigned c, EOT & x) {
        double res = 0;

        for(unsigned k : partial_func_terms[c]) {
            if (x.parity[k])
                res -= eval.walshEval.values[k];
            else
                res += eval.walshEval.values[k];
        }

		return res;
	}
};

#endif