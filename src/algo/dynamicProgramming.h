#ifndef _dynamicProgramming_h
#define _dynamicProgramming_h

#include <algo/moLocalSearch.h>
#include <explorer/moDummyExplorer.h>
#include <continuator/moIterContinuator.h>
#include <eoOp.h>
#include <neighborhood/moDummyNeighbor.h>
#include <perturb/moMonOpPerturb.h>
#include <perturb/moPerturbation.h>
#include <acceptCrit/moAlwaysAcceptCrit.h>
#include <eval/moEval.h>
#include <eoEvalFunc.h>

template<class Neighbor>
class dynamicProgramming: public moLocalSearch<Neighbor> {
public:

    typedef typename Neighbor::EOT EOT;
    typedef moNeighborhood<Neighbor> Neighborhood;

    dynamicProgramming(eoWalshEval<EOT>& _fullEval, int _walshOrder):
            moLocalSearch<Neighbor>(explorer, cont, _fullEval),
            fullEval(_fullEval),
            walshOrder(_walshOrder)
    {}

	/**
	 * Run the local search on a solution
	 * @param _solution the related solution
	 */
	virtual bool operator()(EOT & _solution) {
        std::vector<std::pair<double, std::vector<int>>> state;
        for(int i = 0; i < std::pow(2, (walshOrder - 1)); i++){
            std::vector<int> v;
            state.push_back(std::make_pair(0, v));
        }
        std::pair<double, std::vector<int>> best = solve_dyn(fullEval, state, fullEval.l);
        for(int i = 0; i < _solution.size(); i++){
            _solution[i] = best.second[i] == 1 ? 0 : 1;
        }
        _solution.fitness(best.first);
        return true;
	}

private:
    std::pair<double, std::vector<int>> solve_dyn(eoWalshEval<EOT> f, std::vector<std::pair<double, std::vector<int>>> state, unsigned lag, double mean = 0){
        if(f.n == 0) {
            int ibest = 0;
            int best = state[0].first;
            for (int i = 0; i < state.size(); i++) {
                if (state[i].first < best) {
                    ibest = i;
                    best = state[i].first;
                }
            }
            state[ibest].first += mean;
            /*std::cout << state[ibest].first << " [";
            for (auto s : state[ibest].second){
                std::cout << s << " ";
            }
            std::cout << "]" << std::endl;*/
            return state[ibest];
        } else {
            eoWalshEval<EOT> fnMinusOne;
            fnMinusOne.l = lag;
            eoWalshEval<EOT> fn;
            fn.l = lag;
            for (int i = 0; i < f.coefs.size(); i++) {
                if (!f.coefs[i].size() > 0) {
                    mean = f.values[i];
                } else {
                    bool isCurrent = false;
                    std::vector<unsigned> coefs;
                    for (int j = 0; j < f.coefs[i].size(); j++) {
                        coefs.push_back(f.coefs[i][j] - (f.n + 1 - lag) + walshOrder);
                        if (f.coefs[i][j] == f.n - 1) {
                            isCurrent = true;
                        }
                    }
                    if (isCurrent) {
                        fn.coefs.push_back(coefs);
                        fn.values.push_back(f.values[i]);
                        for (auto c: fn.coefs) {
                            fn.n = std::max(*max_element(c.begin(), c.end()) + 1, fn.n);
                        }
                        fn.p = fn.values.size();
                    } else {
                        fnMinusOne.coefs.push_back(f.coefs[i]);
                        fnMinusOne.values.push_back(f.values[i]);
                        for (auto c: fnMinusOne.coefs) {
                            if (c.size() > 0)
                                fnMinusOne.n = std::max(*max_element(c.begin(), c.end()) + 1, fnMinusOne.n);
                        }
                        fnMinusOne.p = fnMinusOne.values.size();
                    }
                }
            }
            std::vector<double> fitnesses;
            std::vector<std::vector<int>> vectors = generateVectors(fn.n);
            for(auto v : vectors){
                EOT sol;
                for(auto s : v){
                    sol.push_back(s);
                }
                fn(sol);
                fitnesses.push_back(sol.fitness());
            }
            std::vector<std::pair<double, std::vector<int>>> new_state;
            for(int i = 0; i < std::pow(2, (walshOrder - 1)); i++){
                std::vector<int> v;
                new_state.push_back(std::make_pair(0, v));
            }
            if(walshOrder == 1){
                if(fitnesses[0] < fitnesses [1]){
                    std::vector<int> v{1};
                    v.insert(std::end(v), std::begin(state[0].second), std::end(state[0].second));
                    new_state[0] = std::make_pair(state[0].first + fitnesses[0], v);
                } else {
                    std::vector<int> v{-1};
                    v.insert(std::end(v), std::begin(state[0].second), std::end(state[0].second));
                    new_state[0] = std::make_pair(state[0].first + fitnesses[1], v);
                }
            } else {
                for(int i = 0; i < new_state.size(); i++){
                    int istate = i;
                    if(i < new_state.size() / 2){
                        istate = 2 * i;
                    } else {
                        istate = 2 * (i - new_state.size() / 2);
                    }
                    if(state[istate].first + fitnesses[2 * i] < state[istate + 1].first + fitnesses[2 * i + 1]){
                        std::vector<int> v{1};
                        v.insert(std::end(v), std::begin(state[istate].second), std::end(state[istate].second));
                        new_state[i] = std::make_pair(state[istate].first + fitnesses[2 * i], v);
                    } else {
                        std::vector<int> v{-1};
                        v.insert(std::end(v), std::begin(state[istate + 1].second), std::end(state[istate + 1].second));
                        new_state[i] = std::make_pair(state[istate + 1].first + fitnesses[2 * i + 1], v);
                    }
                }
            }
            return solve_dyn(fnMinusOne, new_state, lag, mean);
        }
    }

    std::vector<std::vector<int>> generateVectors(int size) {
        std::vector<std::vector<int>> vectors;
        std::vector<int> v(size, 0);
        while (true) {
            vectors.push_back(v);
            bool flip = true;
            for (int i = size - 1; i >= 0; --i) {
                if (flip) {
                    v[i] = !v[i];
                    if (v[i] == 1) {
                        flip = false;
                    }
                }
            }
            if (flip) {
                break;
            }
        }
        return vectors;
    }

    class dummmyMonOp: public eoMonOp<EOT> {
    public:
        bool operator()(EOT&) {
            return false;
        }
    } dummyOp;

    moTrueContinuator<Neighbor> cont;
    moDummyExplorer<Neighbor> explorer;
    eoWalshEval<EOT>& fullEval;
    int walshOrder;
};

#endif

