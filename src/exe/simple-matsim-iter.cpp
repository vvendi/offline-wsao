#include <EO.h>
#include <ga.h>
#include <eoMatsimEval.h>
#include <bitset>

typedef double Fitness;
typedef eoBit<Fitness> Indi;

int main (int argc, char ** argv) {
    eoRng rng(1);
    std::string prefix;
    if(argc > 2){
        prefix = argv[2];
    }else{
        prefix = "base";
    }
    eoMatsimEval<Indi> eval(prefix, 60);
    std::string bit_string = argv[1];
    std::bitset<108> bitset(bit_string);
    Indi solution;
    for(int i = bitset.size() - 1; i >= 0; i--){
        solution.emplace_back(bitset[i]);
    }
    eval(solution);
    std::cout << solution << std::endl;
    std::fstream log("log.txt", std::fstream::app);
    if(log.is_open()){
        log << solution << std::endl;
        log.close();
    }
}