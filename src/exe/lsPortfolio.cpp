//-----------------------------------------------------------------------------
/** 
 * Portfolio : ILS, and DRILS
 *
 * SV - 2021/02/26 - version 1
 *x@
 */
//-----------------------------------------------------------------------------

//./src/exe/lsPortfolio -I=../instance/walsh.json -a=1 -s=1 --nghType=1 --nghSize=1.0 -k=0.1 -S=1 -t=2 -n=2 --out=out.dat --solutionOut=out.sol

// standard includes
#include <stdexcept>  
#include <iostream>   
#include <fstream>
#include <iomanip>
#include <string.h>

// the general include for eo
#include <EO.h>
#include <utils/eoRNG.h>
#include <utils/eoRndGenerators.h>
#include <utils/eoParser.h>
#include <eoInit.h>

// declaration of the namespace
using namespace std;

//-----------------------------------------------------------------------------
// representation of solutions, and neighbors
#include <moNeighborhoodContainer.h>
#include <moEfficientNeighborhoodContainer.h>
#include <eoWalshSolution.h>                         // bit string + parity
#include <moWalshNeighbor.h> // neighbor of Walsh solution

//-----------------------------------------------------------------------------
// fitness function, and evaluation of neighbors
#include <eoWalshEval.h>
#include <moWalshIncrEval.h>
#include <eval/moFullEvalByModif.h>
#include <moWalshDoubleIncrEval.h>

//-----------------------------------------------------------------------------
// neighborhood description
#include <neighborhood/moRndWithoutReplNeighborhood.h>
#include <neighborhood/moOrderNeighborhood.h>

//-----------------------------------------------------------------------------
// the stopping criteria
#include <continuator/moTimeContinuator.h>
#include <continuator/moTrueContinuator.h>

//-----------------------------------------------------------------------------
// the best improvement Hill-Climbing local search

#include <bestNeighborSelect.h>
#include <worstImprovementNeighborSelect.h>
#include <firstImprovementNeighborSelect.h>

#include <moHCdoubleExplorer.h>
#include <algo/moLocalSearch.h>

//-----------------------------------------------------------------------------
// xover
#include <eoPartitionXover.h>

//-----------------------------------------------------------------------------
// ILS
#include <moDRILSexplorer.h>
// Dynamic Programming
#include <dynamicProgramming.h>

// neighborhood (distance 1, 2, etc.)
#include <distance1NghFactory.h>
#include <distance2NghFactory.h>
#include <rndDistance2NghFactory.h>
#include <impDistance2NghFactory.h>

// acceptance criteria
#include <acceptCrit/moBetterAcceptCrit.h>

// perturbation
//#include <mokBitsPerturb.h>
#include <moWalshPerturb.h>
#include <moVIGPerturb.h>

//Algorithm and its components
#include <algo/moILS.h>
//#include <comparator/moSolNeighborComparator.h>

//-----------------------------------------------------------------------------
// checkpoint
#include <continuator/moCheckpoint.h>
#include <utils/eoFileMonitor.h>
#include <utils/eoTimeCounter.h>
#include <continuator/moCounterStat.h>
#include <continuator/moFitnessStat.h>

#include <eoFlagedMonitor.h>
#include <moImprovementStat.h>

// REPRESENTATION
//-----------------------------------------------------------------------------
// define your individuals

//typedef double Fitness;
typedef eoMinimizingFitness Fitness;
typedef moEfficientNeighborhoodContainer<Fitness> NghContainer ;  // fitness values of all neighbors
typedef eoWalshSolution<NghContainer> Indi;  // 
typedef moWalshNeighbor<Indi> Neighbor ;  // bit string neighbor 

template<>
moWalshIncrEval<Neighbor>* moWalshNeighbor<Indi>::peval = 0;

// Main function
//-----------------------------------------------------------------------------
void main_function(int argc, char **argv)
{
    /* =========================================================
     *
     * Parameters from parser
     *
     * ========================================================= */

    // First define a parser from the command-line arguments
    eoParser parser(argc, argv);

    // random seed parameter
    eoValueParam<uint32_t> seedParam(time(0), "seed", "Random number seed", 'S');
    parser.processParam( seedParam, "Input" );
    unsigned seed = seedParam.value();

    // file instance name
    eoValueParam<string> fileNameParam("../../../instance/sparse_surrogates_1080/reg0.json", "instance", "Filename of the Walsh instance", 'I');
    parser.processParam( fileNameParam, "Input" );
    string instanceFileName = fileNameParam.value();
    
    // local search type
    eoValueParam<uint32_t> algoParam(2, "algo", "Local search algorithm. 0:ILS, 1:DRILS, 2:Dynamic", 'a');
    parser.processParam(algoParam, "Parameter" );
    unsigned int algoType = algoParam.value();

    eoValueParam<uint32_t> walshOrderParam(2, "algo", "Walsh order", 'w');
    parser.processParam(walshOrderParam, "Parameter" );
    unsigned int walshOrder = walshOrderParam.value();

    // Neighborhood selection strategy
    eoValueParam<uint32_t> selectParam(0, "select", "Selection strategy of a neighbor. 0: best improvement, 1: first improvement, 2: worst improvement", 's');
    parser.processParam(selectParam, "Parameter" );
    unsigned int selectionStrategy = selectParam.value();

    eoValueParam<uint32_t> nghTypeParam(1, "nghType", "Neighborhood type. 1:hamming distance 1, 2:Hamming distance 2, 3:Hamming distance 2 with rnd neighbors, 4:Hamming distance 2 with important neighbors", 'D');
    parser.processParam(nghTypeParam, "Parameter");
    unsigned int nghType = nghTypeParam.value();

    eoValueParam<double> nghSizeParam(1, "nghSize", "Neighborhood size (if necessary), ratio to problem dimension n");
    parser.processParam(nghSizeParam, "Parameter");
    double nghSizeRatio = nghSizeParam.value();

    // perturbation
    eoValueParam<double> pertypeParam(0, "pertype", "Perturbation type. 0: rnd classical, 1: VIG perturbation");
    parser.processParam(pertypeParam, "Parameter" );
    unsigned int pertype = pertypeParam.value();

    eoValueParam<double> kParam(2, "strength", "Perturbation strength (number of bit flips, or ratio to n)", 'k');
    parser.processParam(kParam, "Parameter" );
    unsigned int strength;

    // stopping criterium
    eoValueParam<unsigned int> stopParam(1, "time", "Stopping criterium (number of seconds)", 't');
    parser.processParam(stopParam, "Parameter" );
    time_t duration = stopParam.value();

    // file instance name
    eoValueParam<uint32_t> nParam(1, "nbRun", "Number or runs", 'n');
    parser.processParam(nParam, "Output" );
    unsigned int nbRun = nParam.value();

    eoValueParam<string> fileOutParam("out.dat", "out", "Filename of output statistics", 'o');
    parser.processParam( fileOutParam, "Output" );
    string outFileName = fileOutParam.value();

    eoValueParam<string> fileOutSolParam("solution.dat", "solutionOut", "Filename of output with the best solution");
    parser.processParam( fileOutSolParam, "Output" );
    string outSolutionFileName = fileOutSolParam.value();

    // the name of the "status" file where all actual parameter values will be saved
    string str_status = parser.ProgramName() + ".status"; // default value
    eoValueParam<string> statusParam(str_status.c_str(), "status", "Status file");
    parser.processParam( statusParam, "Persistence" );

    // do the following AFTER ALL PARAMETERS HAVE BEEN PROCESSED
    // i.e. in case you need parameters somewhere else, postpone these
    if (parser.userNeedsHelp()) {
        parser.printHelp(cout);
        exit(1);
    }
    /*
    if (statusParam.value() != "") {
        ofstream os(statusParam.value().c_str());
        os << parser;// and you can use that file as parameter file
    }
    */

    /* =========================================================
     *
     * Random seed
     *
     * ========================================================= */

    eoRng rng(seed);

    /* =========================================================
     *
     * Eval fitness function (full evaluation)
     *
     * ========================================================= */

    // the fitness function is the Walsh function
    //eoWalshEval<Indi> eval(instanceFileName.c_str(), minimisation);
    eoWalshEval<Indi> eval(instanceFileName.c_str());

    unsigned n = eval.n;

    if (kParam.value() < 1.0)
        strength = (unsigned int) round(kParam.value() * n);
    else
        strength = (unsigned int) kParam.value();

    /* =========================================================
     *
     * evaluation of a neighbor solution
     *
     * ========================================================= */

    // Use it if there is no incremental evaluation: a neighbor is evaluated by the full evaluation of a solution
    //moFullEvalByModif<Neighbor> neighborEval(eval);

    // Incremental evaluation of the neighbor
    // neighborhood
    std::vector<moBitsNeighbor<Indi> > neighborsVector;

    Distance1NghFactory<moBitsNeighbor<Indi> > nghD1Factory(n);
    Distance2NghFactory<moBitsNeighbor<Indi> > nghD2Factory(n);
    RndDistance2NghFactory<moBitsNeighbor<Indi> > nghRndD2Factory(rng, n, (unsigned) (nghSizeRatio * n));
    ImpDistance2NghFactory<moBitsNeighbor<Indi> > nghImpD2Factory(eval, n, (unsigned) (nghSizeRatio * n));

    switch (nghType) {
        case 1:  
            nghD1Factory(neighborsVector);
            break;

        case 2:
            nghD2Factory(neighborsVector);
            break;

        case 3:
            nghRndD2Factory(neighborsVector);
            break;

        case 4:
            nghImpD2Factory(neighborsVector);
            break;
    }

    /*
    for(auto & n : neighborsVector) {
        cout << n << ", ";
    }
    cout << endl;
    cout << "ok factory " << neighborsVector.size() << endl;
    */


    //  Incremental evaluation of the neighbor
    moWalshIncrEval<Neighbor> neighborEval(eval, neighborsVector);

    moWalshNeighbor<Indi>::peval = &neighborEval;

    /* =========================================================
     *
     * evaluation of a vector of neighbor solution (Double incremental evaluation)
     *
     * ========================================================= */

    // Incremental evaluation of the neighbor
    moWalshDoubleIncrEval<Neighbor> doubleEval(neighborEval);

    /* =========================================================
     *
     * Initialization of the solution
     *
     * ========================================================= */

    eoUniformGenerator<bool> uGen(rng);
    eoInitFixedLength<Indi> random(n, uGen);

    /* =========================================================
     *
     * the neighborhood of a solution
     *
     * ========================================================= */

    // neighborhood : modify one single bit

    //unsigned neighborhoodSize = n ;
    //moRndWithoutReplNeighborhood<Neighbor> neighborhood(rng, neighborhoodSize);

    moOrderNeighborhood<Neighbor> neighborhood(n);

    /* =========================================================
     *
     * continuator
     *
     * ========================================================= */

    // time stopping criterium
    moTimeContinuator<Neighbor> continuator(duration, false);

    /* =========================================================
     *
     * checkpoint and statistics
     *
     * ========================================================= */

    moCheckpoint<Neighbor> checkpoint(continuator);

    eoFileMonitor fmonitor(outFileName, " ", false, true, false);

    moImprovementStat<Indi> imprStat;
    checkpoint.add(imprStat);

    eoFlagedMonitor monitor(imprStat);
    monitor.add(fmonitor);
    checkpoint.add(monitor);

    moCounterStat<Indi> iterCounter;
    checkpoint.add(iterCounter);
    fmonitor.add(iterCounter);

    eoTimeCounter timeStat;
    checkpoint.add(timeStat);
    fmonitor.add(timeStat);

    moFitnessStat<Indi> fitnessStat;
    fitnessStat.setprecision(10);
    checkpoint.add(fitnessStat);
    fmonitor.add(fitnessStat);

    /* =========================================================
     *
     * the HC
     *
     * ========================================================= */

    NeighborSelect<NghContainer> * neighborSelect;

    switch (selectionStrategy) {
        // best improvement
        case 0: neighborSelect = new BestNeighborSelect<NghContainer>();
        break;
        // first improvement
        case 1: neighborSelect = new FirstImprovementNeighborSelect<NghContainer>(rng);
        break;
        // worst improvement
        case 2: neighborSelect = new WorstImprovementNeighborSelect<NghContainer>();
        break;
    }

    // stop on a local optimum
    moTrueContinuator<Neighbor> continuatorHC;
    moCheckpoint<Neighbor> checkpointHC(continuatorHC);

    moCounterStat<Indi> iterHCcounter("hcIter", false);
    checkpointHC.add(iterHCcounter);
    fmonitor.add(iterHCcounter);

    // comparator between solution and neighbor or between neighbors
    moSolNeighborComparator<Neighbor> solNeighborComparator;
   // moEqualSolNeighborComparator<Neighbor> solNeighborComparator;

    moHCdoubleExplorer<Neighbor> explorer(neighborhood, neighborEval, doubleEval, *neighborSelect, solNeighborComparator);

    moLocalSearch<Neighbor> hc(explorer, checkpointHC, eval);

    /* =========================================================
     *
     * Perturbation operator
     *
     * ========================================================= */

    //mokBitsPerturb<Neighbor> perturbation(rng, neighborEval, n, strength);

    //moWalshPerturb<Neighbor> rndPerturbation(rng, neighborEval, doubleEval, strength);

    // vig perturbation
    //moVIGPerturb<Neighbor> perturbation(rng, neighborEval, doubleEval, strength);

    moPerturbation<Neighbor> * perturbation;

    switch (pertype) {
        // random perturbation (classical)
        case 0: perturbation = new moWalshPerturb<Neighbor>(rng, neighborEval, doubleEval, strength);
        break;
        // VIG perturbation
        case 1: perturbation = new moVIGPerturb<Neighbor>(rng, neighborEval, doubleEval, strength);
        break;
    }

    /* =========================================================
     *
     * Partition xover
     *
     * ========================================================= */

    eoPartitionXover<Neighbor> xover(neighborEval);

    /* =========================================================
     *
     * acceptance criteria
     *
     * ========================================================= */

    moBetterAcceptCrit<Neighbor> acceptCrit;

    /* =========================================================
     *
     * the ILS
     *
     * ========================================================= */
    
    //moILS< Neighbor, Neighbor > ils(hc, eval, checkpoint, perturbation, acceptCrit);

    /* =========================================================
     *
     * the DRILS
     *
     * ========================================================= */

    moDRILSexplorer< Neighbor, Neighbor > drilsExplorer(hc, *perturbation, xover, acceptCrit);

    //moLocalSearch<Neighbor> ils(drilsExplorer, checkpoint, eval);

    /* =========================================================
     *
     * the local search
     *
     * ========================================================= */

    moLocalSearch<Neighbor> * algo;

    switch (algoType) {
        // ILS
        case 0: algo = new moILS< Neighbor, Neighbor >(hc, eval, checkpoint, *perturbation, acceptCrit);
        break;
        // DRILS
        case 1: 
            algo = new moLocalSearch<Neighbor>(drilsExplorer, checkpoint, eval);
        break;
        // Dynamic Programming
        case 2:
            algo = new dynamicProgramming<Neighbor>(eval, walshOrder);
        break;
    }

    /* =========================================================
     *
     * executes the local search from a random solution
     *
     * ========================================================= */

    ofstream fileSolution(outSolutionFileName.c_str());

    if (fileSolution.is_open()) {
        for(unsigned int i = 0; i < nbRun; i++) {
            cout << "run " << i << endl;

            // initialization of the file name
            fmonitor.reset(outFileName + "_" + to_string(i));

            // The current solution
            Indi solution;

            // set restart time 
            continuator.init(solution);
            imprStat.reset();
            iterHCcounter.reset();
            timeStat.reset();
            iterHCcounter.reset();

            //checkpoint.init(solution);

            // Apply random initialization
            random(solution);

            // Evaluation of the initial solution:
            eval(solution);

            // Output: the intial solution
            std::cout << "initial: " << solution << std::endl ;

            // Apply the local search on the solution !
            (*algo)(solution);

            // Output: the final solution
            //std::cout << iterCounter.value() << " " ;
            std::cout << "final: " << solution << std::endl ;

            fmonitor();

            fileSolution << std::setprecision(10) << solution << std::endl ;
        }

        fileSolution.close();
    } else {
        cerr << "Impossible to open file: " << outSolutionFileName << endl;
    }

    delete perturbation;
    delete algo;
}

// A main that catches the exceptions

int main(int argc, char **argv)
{
    try {
        main_function(argc, argv);
    }
    catch (exception& e) {
        cout << "Exception: " << e.what() << '\n';
    }
    return 1;
}
