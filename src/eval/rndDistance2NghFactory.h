/*
 <rndDistance2NghFactory.h>
 */

#ifndef _RndDistance2NghFactory_h
#define _RndDistance2NghFactory_h

#include <vector>
#include <utils/eoRNG.h>

#include <neighborhoodFactory.h>

/**
 * Neigbors with Hamming distance = 1,
 * and, some random neighbords with hamming distance 2
 * 
 */
template <class Neighbor>
class RndDistance2NghFactory : NeighborhoodFactory<Neighbor>
{
public:
    RndDistance2NghFactory(eoRng & _rng, unsigned _n, unsigned _nghSize) : rng(_rng), n(_n), nghSize(_nghSize) {
    }

    void operator()(std::vector<Neighbor> & neighborsVector) {        
        // always hamming distance 1
        for(unsigned i = 0; i < n; i++) {
            Neighbor n;
            n.nBits = 1;
            n.bits.push_back(i);

            neighborsVector.push_back(n);
        }    

        // random neighbor from hamming distance 2
        std::vector< pair<unsigned, unsigned> > all(n * (n - 1) / 2);

        unsigned k = 0;
        for(unsigned i = 0; i < n - 1; i++) {
            for(unsigned j = i + 1; j < n; j++) {
                all[k] = {i, j};
                k++;
            }
        }

        for(k = 0; k < nghSize - n; k++) {
            unsigned id = rng.uniform(all.size() - k);

            Neighbor n;
            n.nBits = 2;
            n.bits.push_back(all[id].first);
            n.bits.push_back(all[id].second);
            neighborsVector.push_back(n);   

            // swap with the last one
            pair<unsigned, unsigned> tmp = all[id];
            all[id] = all[all.size() - k - 1];
            all[all.size() - k - 1] = tmp;         
        }

        sort(neighborsVector.begin(), neighborsVector.end(), vector_comp<Neighbor>);
    }

protected:
    eoRng & rng;

    unsigned n;

    unsigned nghSize;
};

#endif
