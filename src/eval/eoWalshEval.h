/*
  walshFunc.h

 Author: 
  Sebastien Verel, 
  Univ. du Littoral Côte d'Opale, France.

 */

#ifndef _eoWalshEval_h
#define _eoWalshEval_h

#include <iostream>
#include <fstream>
#include <sstream> 
#include <vector>
#include <algorithm>

#include <eoEvalFunc.h>
#include "rapidjson/document.h"
#include "rapidjson/istreamwrapper.h"


bool mycomp(const pair<double, vector<unsigned> > & e1, const pair<double, vector<unsigned>> & e2) {
    if (e1.second.size() == e2.second.size()) {
        int i = 0;

        while (i < e1.second.size() and e1.second[i] == e2.second[i]) i++;

        if (i >= e1.second.size())
            return false;
        else
            return e1.second[i] < e2.second[i];
    } else
     return e1.second.size() < e2.second.size();
}

/*
    Walsh function:
      x binary string,
          f(x) = \sum_k w_k phi_k(x)
          f(x) = \sum_k w_k (-1)^{\sum k_i * x_i}
            where x_i and k_i are the i binary digit of x and k
 */
template <class EOT>
class eoWalshEval : public eoEvalFunc<EOT> {
  typedef typename EOT::Fitness Fitness;

public:
    eoWalshEval() : n(0), p(0) { }

    eoWalshEval(std::string _filename) {
        // std::fstream file(_filename.c_str(), std::ios::in);
        const char *ext = ".json";
        size_t xlen = strlen(ext);
        size_t slen = _filename.size();
        std::ifstream file(_filename);
        if (file) {
            if(strcmp(_filename.c_str() + slen - xlen, ext) == 0)
            {
                readJson(file);
            }
            else
            {
                readTxt(file);
            }

            file.close();
        }
        else {
            std::cerr << "Impossible to open file " << _filename << std::endl;
        }
    }

    eoWalshEval(std::string _filename, unsigned int format) {  /* format=0 : json, format=1 text */
        std::ifstream file(_filename);

        if (file) {
            if (format == 0)
                readJson(file);
            else
                readTxt(file);

            file.close();
        }
        else {
            std::cerr << "Impossible to open file " << _filename << std::endl;
        }

    }

    /*
        Compute the fitness value
     */
    void operator()(EOT & x) {
        x.parity.resize(p);

        Fitness res = 0;

        for(unsigned k = 0; k < coefs.size(); k++) {
            // compute the parity of the number of 1 in term k of x
            parity(x, k);
            if (x.parity[k])
                //res -= values[k];
                res = res - values[k];
            else
                //res += values[k];
                res = res + values[k];
        }

        x.fitness(res);
    }

    /*
        Print instance
     */
    void print(unsigned int format = 0) {
        printOn(std::cout, format);
    }

    // Length of the bit string (problem dimension)
    unsigned n;

    // Number of Walsh terms
    unsigned p;

    // Lag if sparse
    unsigned l;

    // Variables of each Walsh terms (vector (of size p) of variable lists)
    std::vector< std::vector<unsigned> > coefs;

    // Values for each coefficients/terms (vector of size p)
    std::vector< double > values;

protected:
    /*
        Parity of the number of 1 in x
            False = 0 = even
            True  = 1 = odd
        Complexity O(k_max)
     */
    void parity(EOT & x, unsigned k) {
        bool phi = false; // even

        std::vector<unsigned> & coef = coefs[k];
        for(unsigned j = 0; j < coef.size(); j++) {
            phi = (phi ? !x[ coef[j] ] : x[ coef[j] ]); // xor
        }

        x.parity[k] = phi;
    }

    /*
          Read instance from in steam (file)

          Format:

          for each term:
          i_1 i_2 ... i_{n_{k}}  w_k
     */
    void readTxt(std::istream& _is)
    {
        std::string line;
        double v;

        n = 0;
        p = 0;

        while ( std::getline( _is, line ) ){
            std::stringstream ss(line);

            if (line.empty())
                continue;
            
            std::vector<double> tmp;

            while(ss >> v)
                tmp.push_back(v);

            ++p;

            std::vector<unsigned> coef;

            for (int i = 0; i < tmp.size() - 1; ++i) {
                if (tmp[i] > n)
                    n = (unsigned) tmp[i];
                coef.push_back( (unsigned) tmp[i] );
            }

            coefs.push_back(coef);

            values.push_back(tmp.back());
        }

        ++n;
    }

    /*
              Read instance from in steam (Jsonfile)

              Format:
              {"problem": {"terms":[{"w": -2, "ids": [0, 1]},...]}}
              for each term:
              w_k w: val
              i_1 i_2 ... i_{n_{k}} ids:[vals]
     */
    int readJson(std::ifstream &ifs){
        rapidjson::IStreamWrapper isw(ifs);

        rapidjson::Document d;
        d.ParseStream(isw);
        assert(d.IsObject());
        const rapidjson::Value& jterms = d["problem"]["terms"];
        
        unsigned xi;
        n = 0;
        p = jterms.Size();
        if(d["problem"].HasMember("l")){
            l = d["problem"]["l"].GetUint();
        } else {
            l = n - 1;
        }

        vector< pair<double, vector<unsigned> > > terms(p);

        double w;
        for (int ind = 0; ind < p; ++ind) {
            std::vector<unsigned> coef(jterms[ind]["ids"].Size());

            for (int j = 0; j < jterms[ind]["ids"].Size(); ++j) {
                xi = jterms[ind]["ids"][j].GetUint();
                if (xi > n)
                    n = xi;
                coef[j] = xi;
            }

            w = jterms[ind]["w"].GetDouble();
            terms[ind] = { w, coef };

            /*
            coefs.push_back(coef);
            values.push_back(w);
            */
        }
        n++;

        // dimension could be larger than the id of variable in the terms
        if (d["problem"].HasMember("n"))
            if (d["problem"]["n"].IsInt())
                if (n < d["problem"]["n"].GetInt())
                    n = d["problem"]["n"].GetInt();

        sort(terms.begin(), terms.end(), mycomp);

        coefs.resize(p);
        values.resize(p);

        for(unsigned i = 0; i < p; i++) {
            values[i] = terms[i].first;
            coefs[i]  = terms[i].second;
        }

        /*
        for(auto & t : terms) {
            cout << "(" << t.first << " [" ;
            for(auto & c : t.second)
                cout << " " << c ;
            cout << "]) ";
        }
        */
       // cout<<n<<"\t"<<coefs.size()<<"\t"<<values[2]<<'\t'<<minimisation<<"\n";

        return 0;

    }

    virtual void printOn(std::ostream& _os, unsigned int format = 0) {
        if (format == 0)
            printOnJson(_os);
        else
            printOnTxt(_os);
    }

    virtual void printOnTxt(std::ostream& _os) {
        //_os << n << " " << p << std::endl;

        for(unsigned k = 0; k < p; k++) {
            if (coefs[k].size() > 0)
                for(unsigned j : coefs[k])
                    _os << j << " " ;

            _os << values[k] ;

            _os << std::endl;
        }
    }

  void printOnJson(std::ostream & _os) {
    _os << "{\"problem\":{" ;

    // terms
    _os << "\"terms\":[";

    bool firstTerm = true;
    for(unsigned int i = 0; i < p; i++) {
      if (!firstTerm)
        _os << ",";
      else
        firstTerm = false;

      _os << "{\"w\":" << values[i] << ",";

      _os << "\"ids\":[" ;

      if (coefs[i].size() > 0)
        _os << coefs[i][0];
      
      for(unsigned int j = 1; j < coefs[i].size(); j++) 
        _os << "," << coefs[i][j];

      _os << "]" ; // end of ids

      _os << "}" ;  // end of term
    }

    _os << "]" ; // end of terms

    _os << "}}" << endl; // end of problems, and json file
  }

};

#endif
