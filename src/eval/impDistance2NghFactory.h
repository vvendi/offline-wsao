/*
 <impDistance2NghFactory.h>
 */

#ifndef _ImpDistance2NghFactory_h
#define _ImpDistance2NghFactory_h

#include <vector>

#include <neighborhoodFactory.h>
#include <eoWalshEval.h>


/**
 * Neigbors with Hamming distance = 1,
 * and, some neighbords at hamming distance 2 with the higest importance
 * 
 */
template <class Neighbor>
class ImpDistance2NghFactory : NeighborhoodFactory<Neighbor>
{
    typedef typename Neighbor::EOT EOT;

public:
    ImpDistance2NghFactory(eoWalshEval<EOT> & _eval, unsigned _n, unsigned _nghSize) : eval(_eval), n(_n), nghSize(_nghSize) {
    }

    void operator()(std::vector<Neighbor> & neighborsVector) {
        // always hamming distance 1
        for(unsigned i = 0; i < n; i++) {
            Neighbor n;
            n.nBits = 1;
            n.bits.push_back(i);

            neighborsVector.push_back(n);
        }    

        // neighbors (at hamming distance 2) with the heightest importance (largest coefficient)
        vector< pair<double, unsigned> > all;

        for(unsigned i = 0; i < eval.coefs.size(); i++) {
            if (eval.coefs[i].size() >= 2) {
                all.push_back({ abs(eval.values[i]), i });
            }
        }

        sort(all.begin(), all.end(), std::greater<>());

        /*        
        for(auto & e : all) {
            cout << e.first ;
            for (auto & b : eval.coefs[ e.second ])
                cout << " " << b ;
            cout << endl;
        }
        */

        unsigned i = 0; 
        unsigned k = 0;
        while (k < nghSize - n and i < all.size()) {
            auto & c = eval.coefs[ all[i].second ];

            if (c.size() == 2) {
                Neighbor n;
                n.nBits = 2;  

                for(auto b : c)
                    n.bits.push_back(b);

                neighborsVector.push_back(n);            
                k++;
            } else {
                // all pairs from the term
                for(unsigned j1 = 0; j1 < c.size() - 1; j1++)
                    for(unsigned j2 = j1 + 1; j2 < c.size(); j2++) {
                        if (k < nghSize - n) {
                            Neighbor n;
                            n.nBits = 2;  
                            n.bits.push_back(c[j1]);
                            n.bits.push_back(c[j2]);

                            neighborsVector.push_back(n);            
                            k++;
                        }
                    }
            }

            i++;
        }

        sort(neighborsVector.begin(), neighborsVector.end(), vector_comp<Neighbor>);
    }

protected:
    eoWalshEval<EOT> & eval;

    unsigned n;

    unsigned nghSize;
};

#endif
