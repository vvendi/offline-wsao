/*
*/

#ifndef _moWalshIncrEval_H
#define _moWalshIncrEval_H

#include <vector>
#include <set>
#include <map>
#include <unordered_map>

#include <eval/moEval.h>
#include <eoWalshEval.h>
#include <problems/bitString/moBitsNeighbor.h>

#include <chrono>
//#include <ctime>    

/**
 * Incremental evaluation Function for the Walsh problem
 */
template< class Neighbor >
class moWalshIncrEval : public moEval<Neighbor>
{
public:
  typedef typename Neighbor::EOT EOT;
  typedef moBitsNeighbor<EOT> BitsNeighbor;
  typedef typename EOT::Fitness Fitness;
  
  /*
   * default constructor
   * @param _walshEval full evaluation of the Walsh problem
   * @param _neihgborhood 
   */
  moWalshIncrEval(eoWalshEval<EOT> & _walshEval, std::vector<BitsNeighbor> & _neighborhood) : walshEval(_walshEval), neighborsVector(_neighborhood) {
    compute_gradients();

    //compute_modifiedTerms();

    //compute_interactions();
  }

  /*
   * Incremental evaluation of the neighbor for the Walsh problem 
   * @param _solution the solution to move (bit string)
   * @param _neighbor the neighbor to consider (of type moWalshNeighbor)
   */
  virtual void operator()(EOT & _solution, Neighbor & _neighbor) {
    unsigned int iflip = _neighbor.index();

    Fitness d = 0;

    for(unsigned k : modifiedTerms[iflip]) {
      if (_solution.parity[k]) 
        //d += 2 * walshEval.values[ k ] ;
      	d = d + 2 * walshEval.values[ k ] ;
      else
	      //d -= 2 * walshEval.values[ k ] ;
        d = d - 2 * walshEval.values[ k ] ;
    }

    // update the vector of neighbors. Notive that the fitness is the difference of fitness.
    //neighborsVector[iflip].fitness(d);

    _neighbor.fitness(_solution.fitness() + d);
  }

  // Modified walsh terms when flipping bits of the corresponding neighbor (vector of size neighborsVector.size)
  std::vector< std::vector<unsigned> > modifiedTerms;

  // Interaction between variables (vector of size neighborhood size) 
  //std::vector< std::set<unsigned> > interactions;
  std::vector< std::map<unsigned, std::vector<unsigned> > > interactions;

  // original Walsh evaluation
  eoWalshEval<EOT> & walshEval;

  // Vector of neighbors (neighborhood vector), neighbors are supposed to be of type moBitsNeighbor.
  std::vector<BitsNeighbor> & neighborsVector;

  // statistic for computation time of the instance
  std::chrono::duration<double> elapsed_seconds ;

protected:
    /*
      Compute data structure for the incremental and double incremental evaluation
    */
  void compute_gradients() {
    //cout << "compute gradients" << endl;
    auto start = std::chrono::system_clock::now();

    // matrix of terms Q[i][j] : terms that contains variable i and j
    vector< vector< vector<unsigned> > > Q(walshEval.n);

    for(unsigned i = 0; i < Q.size(); i++)
      Q[i].resize(walshEval.n);

    // for each variable i, list of bits j such that there is a term with variable i and j
    vector< vector<unsigned> > Qlist(walshEval.n);

    // for each variable i, list of neighbors with the bit i 
    vector< vector<unsigned> > Nlist(walshEval.n);

    // compute data structure
    unsigned k = 0;

    if (walshEval.coefs[k].size() == 0) // constant term
      k++;

    // degree 1 terms
    for(unsigned i = 0; i < Qlist.size(); i++)
      Qlist[i].push_back(i);

    while (k < walshEval.p && walshEval.coefs[k].size() == 1) {
      Q[ walshEval.coefs[k][0] ][ walshEval.coefs[k][0] ].push_back(k);

      k++;
    }

    // degree 1+ terms 
    while (k < walshEval.p) {
      if (walshEval.coefs[k].size() == 2) {
        Qlist[ walshEval.coefs[k][0] ].push_back(walshEval.coefs[k][1]);
        Qlist[ walshEval.coefs[k][1] ].push_back(walshEval.coefs[k][0]);
        Q[ walshEval.coefs[k][0] ][ walshEval.coefs[k][1] ].push_back(k);
        Q[ walshEval.coefs[k][1] ][ walshEval.coefs[k][0] ].push_back(k);
      } else {
        for(unsigned i = 0; i < walshEval.coefs[k].size() - 1; i++)
          for(unsigned j = i + 1; i < walshEval.coefs[k].size(); j++) {
            if (Q[ walshEval.coefs[k][i] ][ walshEval.coefs[k][j] ].size() == 0) {
              Qlist[ walshEval.coefs[k][i] ].push_back(walshEval.coefs[k][j]);
              Qlist[ walshEval.coefs[k][j] ].push_back(walshEval.coefs[k][i]);              
            }
            Q[ walshEval.coefs[k][i] ][ walshEval.coefs[k][j] ].push_back(k);
            Q[ walshEval.coefs[k][j] ][ walshEval.coefs[k][i] ].push_back(k);
          }
      }

      k++;
    }

    for(unsigned i = 0; i < neighborsVector.size(); i++) {
      for(unsigned k = 0; k < neighborsVector[i].bits.size(); k++)
        Nlist[ neighborsVector[i].bits[k] ].push_back(i);
    }

    /*
    for(unsigned i = 0; i < Nlist.size(); i++) {
      cout << i << ": ";
      for(unsigned ni : Nlist[i])
        cout << ni << " ";
      cout << endl;
    }
    */

    // Compute the terms modified by incremental evaluation (gradient)
    modifiedTerms.resize(neighborsVector.size());

    for(unsigned id_n = 0; id_n < neighborsVector.size(); id_n++) {
      if (neighborsVector[id_n].bits.size() == 1) {
        unsigned i = neighborsVector[id_n].bits[0];

        for(unsigned j : Qlist[i]) {
          for(unsigned k : Q[i][j])
            modifiedTerms[id_n].push_back(k); // attention ne marche pas tout à fait lorsque les termes sont de degré > 2: risque de doublons
        }
      } else { // neighborsVector[id_n].bits.size() == 2
        unsigned i1 = neighborsVector[id_n].bits[0];
        unsigned i2 = neighborsVector[id_n].bits[1];

        for(unsigned j1 : Qlist[i1]) {
          if (j1 != i2)
            for(unsigned k : Q[i1][j1])
              modifiedTerms[id_n].push_back(k);
        }

        for(unsigned j2 : Qlist[i2]) {
          if (j2 != i1)
            for(unsigned k : Q[i2][j2])
              modifiedTerms[id_n].push_back(k);
        }
      }
    }

    //cout << "Gradient DONE" << endl;
    //cout << "double gradient now !" << endl;

    // compute the terms modified terms in the modifiedTerms : the double gradient
    interactions.resize(neighborsVector.size());

    for(unsigned id_n = 0; id_n < neighborsVector.size(); id_n++) {
      //cout << id_n << endl;
      //for(unsigned u : neighborsVector[id_n].bits) {
      if (neighborsVector[id_n].bits.size() == 1) {
        unsigned u = neighborsVector[id_n].bits[0];

        for(unsigned i : Qlist[u]) {
          if (Nlist[i].size() > 0) {
            if (u != i) {
              unsigned ni = 0;
              if (neighborsVector[ Nlist[i][0] ].bits.size() == 1) { // if there a neighbors (i)
                for(unsigned k : Q[i][u]) {
                  //cout << "u insert " << id_n << " " << Nlist[i][0] << " " << k << endl;
                  interactions[id_n][ Nlist[i][0] ].push_back(k);
                }
                ni = 1;
              }

              while (ni < Nlist[i].size()) {
                unsigned id_n1 = Nlist[i][ni];
                unsigned i2 = neighborsVector[id_n1].bits[0];
                if (i2 == i) // neighbor (i, i2)
                  i2 = neighborsVector[id_n1].bits[1];

                //cout << u << " " << i << " " << id_n1 << " " << i2 << endl ;
                if (u != i2) // the term [i, u] does not exist for the neighbor (i, i2) when u == i2
                  for(unsigned k : Q[i][u]) {
                    //cout << "insert " << id_n << " " << id_n1 << " " << k << endl;
                    interactions[id_n][id_n1].push_back(k);
                  }

                ni++;
              }
            } else { // u == i
              unsigned ni = 0;
              if (neighborsVector[ Nlist[u][0] ].bits.size() == 1) { // if there a neighbors (i)
                for(unsigned j : Qlist[u])
                  for(unsigned k : Q[u][j]) {
                    //cout << "u insert " << id_n << " " << Nlist[u][0] << " " << k << endl;
                    interactions[id_n][ Nlist[u][0] ].push_back(k);
                  }
                ni = 1;
              }

              while (ni < Nlist[u].size()) {
                unsigned id_n1 = Nlist[u][ni];
                unsigned i2 = neighborsVector[id_n1].bits[0];
                if (i2 == u)
                  i2 = neighborsVector[id_n1].bits[1];

                for(unsigned j : Qlist[u]) {
                  //cout << "-" << u << " " << j << " " << id_n1 << " " << i2 << endl ;
                  if (j != i2)
                    for(unsigned k : Q[u][j]) {
                      //cout << "insert " << id_n << " " << id_n1 << " " << k << endl;
                      interactions[id_n][id_n1].push_back(k);
                    }
                }

                ni++;
              }
            }
          }
        } 
      } else { // neighborsVector[id_n].bits.size() == 2
        for(unsigned id_u = 0; id_u < 2; id_u++) {
          unsigned u = neighborsVector[id_n].bits[id_u];
          unsigned v = neighborsVector[id_n].bits[1 - id_u];

          for(unsigned i : Qlist[u]) {
            if (Nlist[i].size() > 0) {
              if (u != i) {
                unsigned ni = 0;
                if (neighborsVector[ Nlist[i][0] ].bits.size() == 1) { // if there a neighbors (i)
                  if (i != v)
                    for(unsigned k : Q[i][u]) {
                      //cout << "u insert " << id_n << " " << Nlist[i][0] << " " << k << endl;
                      interactions[id_n][ Nlist[i][0] ].push_back(k);
                    }
                  ni = 1;
                }

                while (ni < Nlist[i].size()) {
                  unsigned id_n1 = Nlist[i][ni];
                  unsigned i2 = neighborsVector[id_n1].bits[0];
                  if (i2 == i)
                    i2 = neighborsVector[id_n1].bits[1];

                  //cout << u << " " << i << " " << id_n1 << " " << i2 << endl ;
                  if (u != i2 && i != v)
                    for(unsigned k : Q[i][u]) {
                      //cout << "insert " << id_n << " " << id_n1 << " " << k << endl;
                      interactions[id_n][id_n1].push_back(k);
                    }

                  ni++;
                }
              } else { // u == i
                unsigned ni = 0;
                if (neighborsVector[ Nlist[u][0] ].bits.size() == 1) { // if there a neighbors (i)
                  for(unsigned j : Qlist[u])
                    if (j != v)
                      for(unsigned k : Q[u][j]) {
                        //cout << "u insert " << id_n << " " << Nlist[u][0] << " " << k << endl;
                        interactions[id_n][ Nlist[u][0] ].push_back(k);
                      }
                  ni = 1;
                }

                while (ni < Nlist[u].size()) {
                  unsigned id_n1 = Nlist[u][ni];
                  unsigned i2 = neighborsVector[id_n1].bits[0];
                  if (i2 == u)
                    i2 = neighborsVector[id_n1].bits[1];

                  for(unsigned j : Qlist[u]) {
                    //cout << "-" << u << " " << j << " " << id_n1 << " " << i2 << endl ;
                    if (j != i2 && j != v)
                      for(unsigned k : Q[u][j]) {
                        //cout << "insert " << id_n << " " << id_n1 << " " << k << endl;
                        interactions[id_n][id_n1].push_back(k);
                      }
                  }

                  ni++;
                }
              }
            }
          } 
        }

      }
    }

    // stats
    //cout << "end of compute_gradients" << endl;
    auto end = std::chrono::system_clock::now();

    //std::chrono::duration<double> 
    elapsed_seconds = end - start;
    //std::time_t end_time = std::chrono::system_clock::to_time_t(end);

    // std::cout << "1 finished computation at " << std::ctime(&end_time) << "elapsed time: " << elapsed_seconds.count() << "s\n";
  }

};

#endif

