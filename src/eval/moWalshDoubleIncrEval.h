/*
*/

#ifndef _moWalshDoubleIncrEval_H
#define _moWalshDoubleIncrEval_H

#include <vector>
#include <set>

#include <eval/moEval.h>
#include <eoWalshEval.h>
#include <moWalshIncrEval.h>
#include <moNeighborhoodContainer.h>
#include <neighborhood/moOrderNeighborhood.h>

/**
 * Double incremental evaluation Function for the Walsh problem
 */
template< class Neighbor >
class moWalshDoubleIncrEval
{
public:
  typedef typename Neighbor::EOT EOT;
  typedef typename EOT::Fitness Fitness;
  
  /*
   * default constructor
   * @param _ubqpEval full evaluation of the UBQP problem
   */
  moWalshDoubleIncrEval(moWalshIncrEval<Neighbor> & _incrEval) : incrEval(_incrEval) {

  }

  /*
   * Update the evaluation of all neighbors for the Walsh problem
   *
   * @param _solution the solution to move (bit string)
   * @param _neighbor the neighbor to consider (of type moWalshNeighbor)
   */
  //virtual void operator()(EOT & _solution, Neighbor & _neighbor, vector<Neighbor> & _deltas) {
  virtual void operator()(EOT & _solution, Neighbor & _neighbor) { //, moNeighborhoodContainer<Fitness> & _nghContainer) {
    unsigned int i2 = _neighbor.index();

    // Compute new values of neighbors
    Fitness d;

    for(auto & m2 : incrEval.interactions[i2]) {    
      d = 0;

      for(unsigned k : m2.second) {
        if (_solution.parity[k]) 
            //d -= 4 * incrEval.walshEval.values[ k ];
            d = d - 4 * incrEval.walshEval.values[ k ];
        else
            d = d + 4 * incrEval.walshEval.values[ k ];
      }

      _solution.neighborhoodContainer.fitness(m2.first, _solution.neighborhoodContainer.fitness(m2.first) + d);
    }

    /*
    unsigned t = 0;
    for(unsigned i1 : incrEval.interactions[i2]) {
      d = 0;
      
      for(unsigned k : incrEval.nablaSquareTerms[i2][t]) {
      //for(unsigned k : incrEval.nablaSquareTerms[i1][i2]) {
        if (_solution.parity[k]) 
            d -= 4 * incrEval.walshEval.values[ k ];
        else
            d += 4 * incrEval.walshEval.values[ k ];
      }

      _solution.neighborhoodContainer.fitness(i1, _solution.neighborhoodContainer.fitness(i1) + d);

      t++;
    }
    */

    /*
    for(unsigned k : incrEval.modifiedTerms[i2]) {
        for(unsigned i1 : incrEval.termToNeighbors[ k ]) {
            if (_solution.parity[k]) 
                //incrEval.neighborsVector[i1].fitness(incrEval.neighborsVector[i1].fitness() - 4 * incrEval.walshEval.values[ k ]);
                _nghContainer.fitness(i1, _nghContainer.fitness(i1) - 4 * incrEval.walshEval.values[ k ]);
            else
                //incrEval.neighborsVector[i1].fitness(incrEval.neighborsVector[i1].fitness() + 4 * incrEval.walshEval.values[ k ]);
                _nghContainer.fitness(i1, _nghContainer.fitness(i1) + 4 * incrEval.walshEval.values[ k ]);
        }
    }
    */

    // neighborhood container is up to date
    _solution.neighborhoodContainer.validate();
  }

  /*
   * Update the evaluation of all neighbors for the Walsh problem
   * update all neighbors using incremental evaluation
   *
   * @param _solution the solution to move (bit string)
   */
  virtual void operator()(EOT & _solution) { //, moNeighborhoodContainer<Fitness> & _nghContainer) {
    if (_solution.neighborhoodContainer.invalid()) {
      _solution.neighborhoodContainer.resize(incrEval.neighborsVector.size());
      moOrderNeighborhood<Neighbor> neighborhood(_solution.neighborhoodContainer.size());

      Neighbor neighbor;

      neighborhood.init(_solution, neighbor);
      incrEval(_solution, neighbor);
      _solution.neighborhoodContainer.fitness(neighbor.index(), neighbor.fitness() - _solution.fitness());

      while (neighborhood.cont(_solution)) {
        neighborhood.next(_solution, neighbor);

        incrEval(_solution, neighbor);
        _solution.neighborhoodContainer.fitness(neighbor.index(), neighbor.fitness() - _solution.fitness());
      }

      _solution.neighborhoodContainer.validate();
    }
  }

  virtual unsigned size() {
    return incrEval.neighborsVector.size();
  }

private:
  moWalshIncrEval<Neighbor> & incrEval;
  
};

#endif

