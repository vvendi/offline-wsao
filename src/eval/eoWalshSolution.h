#ifndef _eoWalshSolution_h
#define _eoWalshSolution_h

#include "ga/eoBit.h"
#include <moNeighborhoodContainer.h>

template <class NghContainerType>
class eoWalshSolution: public eoBit<typename NghContainerType::Fitness>
{
public:
    typedef NghContainerType NghContainer;
    typedef typename NghContainer::Fitness Fitness;

    // parity of each term of the Walsh expansion
    std::vector<bool> parity;

    // fitness values "vector" of the neighbors
    NghContainer neighborhoodContainer;

    eoWalshSolution() : eoBit<Fitness>() {
    }

    eoWalshSolution(unsigned _size) : eoBit<Fitness>(_size) {        
    }

    eoWalshSolution(const eoWalshSolution & _solution) : eoBit<Fitness>(_solution) {
        // copy the parity vector, and container
        if (!_solution.invalid()) {
            parity.resize(_solution.parity.size());
            for(unsigned k = 0; k < _solution.parity.size(); k++)
                parity[k] = _solution.parity[k];
        }

        if (_solution.neighborhoodContainer.invalid()) 
            neighborhoodContainer.invalidate();
        else 
            neighborhoodContainer.NghContainer::operator=(_solution.neighborhoodContainer);
    }

    eoWalshSolution& operator=(const eoWalshSolution & _solution) {
        eoBit<Fitness>::operator=(_solution);

        // copy the parity vector
        if (!_solution.invalid()) {
            parity.resize(_solution.parity.size());
            for(unsigned k = 0; k < _solution.parity.size(); k++)
                parity[k] = _solution.parity[k];
        }

        if (_solution.neighborhoodContainer.invalid()) 
            neighborhoodContainer.invalidate();
        else 
            neighborhoodContainer = _solution.neighborhoodContainer;            

        return *this;
    }

    virtual void printOn(std::ostream & _os) const {
        eoBit<Fitness>::printOn(_os) ;

        //neighborhoodContainer.printOn(_os);
    }

};

#endif
