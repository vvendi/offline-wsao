/*
 <moBitNeighbor.h>
 */

#ifndef _walshNeighbor_h
#define _walshNeighbor_h

#include <eoWalshSolution.h>
//#include <ga/eoBit.h>
#include <neighborhood/moBackableNeighbor.h>
#include <neighborhood/moIndexNeighbor.h>
#include <problems/bitString/moBitsNeighbor.h>
#include <moWalshIncrEval.h>

/**
 * Neighbor related to a vector of Bit
 */
template< class EOType >
class moWalshNeighbor : public moBackableNeighbor<EOType>, public moIndexNeighbor<EOType>
{
public:

	typedef EOType EOT;
	//typedef typename EOType::Fitness Fitness;

	static moWalshIncrEval<moWalshNeighbor<EOT> >* peval ;

	using moBackableNeighbor<EOT>::fitness;
	using moIndexNeighbor<EOT>::key;
	using moNeighbor<EOT>::invalidate;
	using moNeighbor<EOT>::invalid;

	/**
	 * move the solution
	 * @param _solution the solution to move
	 */
	virtual void move(EOT & _solution) {
		//_solution[key] = !_solution[key];
		(*peval).neighborsVector[key].move(_solution);

		for(unsigned k : (*peval).modifiedTerms[key]) {
		  _solution.parity[k] = !_solution.parity[k];
		}

		_solution.invalidate();   
	}

	/**
	 * move back the solution (useful for the evaluation by modif)
	 * @param _solution the solution to move back
	 */
	virtual void moveBack(EOT & _solution) {
		move(_solution);
	}

	/**
	 * return the class name
	 * @return the class name as a std::string
	 */
	virtual std::string className() const {
		return "moWalshNeighbor";
	}

	/**
	 * Read object.\
     * Calls base class, just in case that one had something to do.
	 * The read and print methods should be compatible and have the same format.
	 * In principle, format is "plain": they just print a number
	 * @param _is a std::istream.
	 * @throw runtime_std::exception If a valid object can't be read.
	 */
	virtual void readFrom(std::istream& _is) {
		std::string fitness_str;
		int pos = _is.tellg();
		_is >> fitness_str;

		if (fitness_str == "INVALID") {
			invalidate();
//			throw std::runtime_error("invalid fitness");
		} else {
			typename EOT::Fitness repFit;
			_is.seekg(pos);
			_is >> repFit;
			fitness(repFit);
		}

		_is >> key;
	}

	/**
	 * Write object. Called printOn since it prints the object _on_ a stream.
	 * @param _os A std::ostream.
	 */
	virtual void printOn(std::ostream& _os) const {
		if (this->invalid())
			_os << "INVALID " << key ;
		else
			_os << fitness() << ' ' << key ;
	}
};

#endif
