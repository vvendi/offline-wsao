/*
 <moEfficientNeighborhoodContainer.h>
 */

#ifndef _EfficientNeighborhoodContainer_h
#define _EfficientNeighborhoodContainer_h

#include <vector>

#include <moNeighborhoodContainer.h>
#include <bst.h>

/**
 * Contain all fitness values of the neighborhoo
 * Supposed to be finite, and indexed neighborhood
 */
template< class Fitness >
class moEfficientNeighborhoodContainer : public moNeighborhoodContainer<Fitness>
{
    using moNeighborhoodContainer<Fitness>::neighborsFitness;

public:
    using moNeighborhoodContainer<Fitness>::fitness;
    using moNeighborhoodContainer<Fitness>::invalidate;
    using moNeighborhoodContainer<Fitness>::invalid;

    moEfficientNeighborhoodContainer() : moNeighborhoodContainer<Fitness>() {
        init(0);
    }

    moEfficientNeighborhoodContainer(unsigned int _size) : moNeighborhoodContainer<Fitness>(_size) {
        init(_size);
    }

    moEfficientNeighborhoodContainer(const moEfficientNeighborhoodContainer<Fitness> & _container) : moNeighborhoodContainer<Fitness>(_container) {
        if (!_container.invalid()) {
            bst = _container.bst;
        }
    }

    moEfficientNeighborhoodContainer& operator=(const moEfficientNeighborhoodContainer<Fitness> & _container) {
        moNeighborhoodContainer<Fitness>::operator=(_container);

        if (_container.invalid()) {
            bst.makeEmpty();
        } else {
            bst = _container.bst;
        }

        return *this;
    }

    virtual void init(unsigned _size) {
        zeroFitness = 0;
        neighborsFitness.resize(_size);
        bst.makeEmpty();

        invalidate();        
    }

    /**
     * Return the fitness value of the neighbor
     *
     * @param i index of the neighbor
     */
    virtual void fitness(unsigned _i, Fitness _value) {
        if (zeroFitness < fitness(_i)) 
            bst.remove(fitness(_i), _i);

        neighborsFitness[_i] = _value;

        if (zeroFitness < _value)
            bst.insert(_value, _i);
    }

    virtual int maximum() {
        Fitness fit;
        int i;

        // becareful: only one 'first' maximum (not a random one)
        bst.maximum(fit, i);

        return i;
    }

    virtual int worstImproving() {
        Fitness fit;
        int i;

        // becareful: only one 'first' minimum (not a random one)
        bst.minimum(fit, i);

        return i;
    }

    /*
        return an improving neighbor, according to the (random) number x between [0, 1]

        @param x number between 0.0, and 1.0
    */
    virtual int improving(double x) {
        Fitness fit;
        int i;

        bst.random(x, fit, i);

        return i;
    }

    virtual void printOn(std::ostream& _os) const {
        if (invalid()) 
            _os << " INVALID";
        else
            _os << " VALID";

        for(Fitness f : neighborsFitness) {
            _os << " " << f;
        }

        _os << " ";

        bst.print(_os);
    }

    /**
     * return the class name
     * @return the class name as a std::string
     */
    virtual std::string className() const {
        return "moEfficientNeighborhoodContainer";
    }


protected:
    BST<Fitness> bst;

    Fitness zeroFitness;
};

#endif
