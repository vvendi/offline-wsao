/*
 <moNeighborhoodContainer.h>
 */

#ifndef _NeighborhoodContainer_h
#define _NeighborhoodContainer_h

#include <vector>

/**
 * Contain all fitness difference values of the neighborhood
 * Supposed to be finite, and indexed neighborhood
 *
 * See also moEfficientNeighborhoodContainer.h to understand
 */
template< class FitnessType >
class moNeighborhoodContainer 
{
public:
    typedef FitnessType Fitness;

    moNeighborhoodContainer() {
        init(0);
    }

    moNeighborhoodContainer(unsigned int _size) {
        init(_size);
    }

    moNeighborhoodContainer(const moNeighborhoodContainer<Fitness> & _container) {
        invalidFitness = _container.invalidFitness;

        neighborsFitness.resize(_container.neighborsFitness.size());

        if (!_container.invalid()) {
            for(unsigned i = 0; i < _container.neighborsFitness.size(); i++)
                neighborsFitness[i] = _container.neighborsFitness[i];
        }
    }

    moNeighborhoodContainer& operator=(const moNeighborhoodContainer<Fitness> & _container) {
        invalidFitness = _container.invalidFitness;

        neighborsFitness.resize(_container.neighborsFitness.size());

        if (!_container.invalid()) {
            for(unsigned i = 0; i < _container.neighborsFitness.size(); i++)
                neighborsFitness[i] = _container.neighborsFitness[i];
        }

        return *this;
    }

    virtual void init(unsigned _size) {
        neighborsFitness.resize(_size);
        invalidate();        
    }

    /**
     * Return the fitness value of the neighbor
     *
     * @param i index of the neighbor
     */
    virtual Fitness fitness(unsigned _i) const {
        return neighborsFitness[_i];
    }

    /**
     * Return the fitness value of the neighbor
     *
     * @param i index of the neighbor
     */
    virtual void fitness(unsigned _i, Fitness _value) {
        neighborsFitness[_i] = _value;
    }

    unsigned size() const {
        return neighborsFitness.size();
    }

    void resize(unsigned int _size) {
        neighborsFitness.resize(_size);
    }

    virtual int maximum() {
        int imax = 0;

        for(unsigned int i = 1; i < neighborsFitness.size(); i++)
            if (neighborsFitness[imax] < neighborsFitness[i])
                imax = i;

        return imax;
    }

    virtual int worstImproving() {
        int imin = -1;
        Fitness zero = 0;

        unsigned i = 0;
        while (i < neighborsFitness.size() && neighborsFitness[i] <= zero)
            i++;

        if (i < neighborsFitness.size()) {
            imin = i;
            for(i = imin + 1; i < neighborsFitness.size(); i++)
                if (zero < neighborsFitness[i] && neighborsFitness[i] < neighborsFitness[imin])
                    imin = i;
        }

        return imin;
    }

    /*
        return an improving neighbor, according to the (random) number x between [0, 1]

        @param x number between 0.0, and 1.0
    */
    virtual int improving(double x) {
        if (0 <= x && x <= 1) {
            Fitness zero = 0;
            unsigned n = 0;
            std::vector<unsigned> imp(neighborsFitness.size());

            for(unsigned int i = 0; i < neighborsFitness.size(); i++)
                if (zero < neighborsFitness[i]) {
                    imp[n] = i;
                    n++;
                }

            if (n == 0)
                return -1;
            else 
                return imp[(unsigned) (x * n)];
        } else
            return -1;
    }

    // Set the vector of neighbors as invalid.
    void invalidate() { 
        invalidFitness = true;  
    }

    // Set the vector of neighbors as valid.
    void validate() { 
        invalidFitness = false;  
    }

    /** 
    * Return true If vector of neighbors is invalid, false otherwise.
    *  @return true If vector of neighbors is invalid.
    */
    bool invalid() const { 
        return invalidFitness; 
    }

    virtual void printOn(std::ostream& _os) const {
        if (invalid()) 
            _os << " INVALID";
        else
            _os << " VALID";

        for(Fitness f : neighborsFitness) {
            _os << " " << f;
        }
    }

    /**
     * return the class name
     * @return the class name as a std::string
     */
    virtual std::string className() const {
        return "moNeighborhoodContainer";
    }

protected:
    std::vector<Fitness> neighborsFitness;

    // true if the fitness of neighbors vector is invalid
    bool invalidFitness;  
};

#endif
