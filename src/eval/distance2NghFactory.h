/*
 <distance2NghFactory.h>
 */

#ifndef _Distance2NghFactory_h
#define _Distance2NghFactory_h

#include <vector>

#include <neighborhoodFactory.h>

/**
 * Neigbors with Hamming distance <= 2
 * 
 */
template <class Neighbor>
class Distance2NghFactory : NeighborhoodFactory<Neighbor>
{
public:
    Distance2NghFactory(unsigned _n) : n(_n) {
    }

    void operator()(std::vector<Neighbor> & neighborsVector) {        
        // hamming distance 1
        for(unsigned i = 0; i < n; i++) {
            Neighbor n;
            n.nBits = 1;
            n.bits.push_back(i);

            neighborsVector.push_back(n);
        }    

        // hamming distance 2
        for(unsigned i = 0; i < n - 1; i++) {
            for(unsigned j = i + 1; j < n; j++) {
                Neighbor n;
                n.nBits = 2;
                n.bits.push_back(i);
                n.bits.push_back(j);

                neighborsVector.push_back(n);
            }
        }
    }

protected:
    unsigned n;
};

#endif
