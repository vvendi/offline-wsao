/*
 <distance1NghFactory.h>
 */

#ifndef _Distance1NghFactory_h
#define _Distance1NghFactory_h

#include <vector>

#include <neighborhoodFactory.h>

/**
 * 
 * 
 */
template <class Neighbor>
class Distance1NghFactory : NeighborhoodFactory<Neighbor>
{
public:
    Distance1NghFactory(unsigned _n) : n(_n) {
    }

    void operator()(std::vector<Neighbor> & neighborsVector) {
        // hamming distance 1
        for(unsigned i = 0; i < n; i++) {
            Neighbor n;
            n.nBits = 1;
            n.bits.push_back(i);

            neighborsVector.push_back(n);
        }    
    }

protected:
    unsigned n;
};

#endif
