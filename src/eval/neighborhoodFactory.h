/*
 <neighborhoodFactory.h>
 */

#ifndef _NeighborhoodFactory_h
#define _NeighborhoodFactory_h

#include <vector>

template <class Neighbor>
bool vector_comp(const Neighbor& l, const Neighbor& r) {
if (l.bits.size() < r.bits.size())
  return true;
else 
  if (l.bits.size() > r.bits.size())
    return false;
  else {
    // equal size
    unsigned i = 0; 

    while (i < l.bits.size() and l.bits[i] == r.bits[i]) 
      i++;

    return (i < l.bits.size() and l.bits[i] < r.bits[i]);
  }
}


/**
 * 
 * 
 */
template <class Neighbor>
class NeighborhoodFactory 
{
public:
    NeighborhoodFactory() {
    }

    virtual void operator()(std::vector<Neighbor> & neighborsVector) = 0;

};

#endif
