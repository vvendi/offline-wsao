/*
  eoMatsimEval.h

 Author: 
  Valentin Vendi,
  Univ. du Littoral Côte d'Opale, France.

 */

#ifndef _eoMatsimEval_h
#define _eoMatsimEval_h

#include <iostream>
#include <fstream>
#include <sstream> 
#include <vector>
#include <set>

#include <eoEvalFunc.h>
#include <simulator.cpp>
#include <thread>
#include <random>

template <class EOT>
class eoMatsimEval : public eoEvalFunc<EOT> {
public:
    eoMatsimEval(const std::string& prefix, int nbIters) {
        simulator = Simulator(prefix, nbIters);
        n = simulator.getVector().size();
    }

    // cpy constructor
    eoMatsimEval(const eoMatsimEval<EOT> & _other) : eoMatsimEval<EOT>() {
        n = _other.n;
        simulator = _other.simulator;
    }

    /*
        Compute the fitness value
     */
    void operator()(EOT & x) {
        simulator.setFromVector(x);
        simulator.eval();
        x.fitness(simulator.getModeTraveltime("pt"));
        std::cout << x << std::endl;
    }

    /*
        Print instance on the output
     */
    void print() const {
        printOn(std::cout);
    }

    // Length of the bit string (problem dimension)
    unsigned n;

    Simulator simulator;

protected:
    virtual void printOn(std::ostream& _os) {
        _os << n << " stops" << std::endl;

        for(auto s : simulator.getVector()) {
            _os << s << " ";
        }
        _os << std::endl;
    }
};

#endif
