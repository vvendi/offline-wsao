
Minimal version of Paradiseo without cpp files, and global variables


Author: Sébastien Verel

Initial version 0.0.0 - 2018/11/03

-----------------------------------------------------------------------------
Notes for the initial version 0.0.0  (Ugly...)


*** From Paradiseo root directory:
- remove eoCtrlCContinue.h and eoCtrlCContinue.cpp
- remove eoSIGContinue.h and eoSIGContinue.cpp
- remove eoScalarFitnessAssembled.h and eoScalarFitnessAssembled.cpp

- The destructor eoFunctorStore::~eoFunctorStore() defined in eoFunctorStore.cpp is moved in the header eoFunctorStore.h
- eoPersistent.cpp : move the definition of operator>> in the header file eoPersistent.h

*** Move all files related with PSO in a new directory pso. (34 files)

*** In es directory:
- remove all the directory in this initial version

*** In ga directory:
- delete all make_*.cpp and associated *.h

*** In utils directory:
- remove eoData.cpp (empty)
- remove eoGnuplot*
- put definitions of eoFileMonitor.cpp into eoFileMonitor.h
- remove eoIntBounds.h, eoRealBounds.h, eoRealVectorBounds.h : very long files to be redesigned. There are linked to ES, so I remove at this moment.
- remove eoLogger.h eoLoger.cpp : useful files to log everything, but in this version, global variable. Need to be a little bit redesigned latter.
- remove eoOStreamMonitor.cpp and put it the header file without lines 69, and 96 to remove log.
- remove file eoParallel.cpp, and eoParallel.h : there is global variable eo:parallel to be removed in a next version.
-remove pipecom.cpp, and pipecom.h : maybe too old code, and not very used. It has been been updated with openmp (see eoParallel.cpp)
- remove eoSignal.h and eoSignal.cpp : there is a global variable
- remove eoState.cpp and copy into the header file eoState.h
- remove eoUpdater.cpp and copy it into the header file eoUpdater.h
- move (rename) make_helper.cpp into make_helper.h : no need in cpp because no make_*.cpp is used in this version
- eoRng.cpp : remove this file, and remove global function random, normal in eoRng.h. Remove the static for K, N, M, but keep const

In eoVector : remove include eoLOgger and lines 87, 88
In eoParser.h : comment line 587 with log
In eoRndGenerators.h : line 42 remove using eoRng, remove "= rng" in every constructors and the position of the argument
In rnd_generators.h : same work like in eoRndGenerators.h (duplicate functions???)
In eoOp.h : don't flip the coin to select in eoQuad2BinOp, remove the rng.flip (line 177)
In eoInit.h : add eoRng in the constructor

in problems:
- add eoRng into nklandscapes

In moeo:
2020/02/19: Removed core/moeoObjectiveVectorTraits.cpp
Now we have to add in cpp using moeo:
   unsigned int moeoObjectiveVectorTraits::nObj;
   std::vector < bool > moeoObjectiveVectorTraits::bObj;
