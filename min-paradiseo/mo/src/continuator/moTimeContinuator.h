/*
<moTimeContinuator.h>
Copyright (C) DOLPHIN Project-Team, INRIA Lille - Nord Europe, 2006-2010

Sébastien Verel, Arnaud Liefooghe, Jérémie Humeau

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  ue,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.
The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.

ParadisEO WebSite : http://paradiseo.gforge.inria.fr
Contact: paradiseo-help@lists.gforge.inria.fr
*/

#ifndef _moTimeContinuator_h
#define _moTimeContinuator_h

#include <chrono>
//#include <time.h>
//#include <ctime>

#include <continuator/moContinuator.h>

/**
 * Termination condition until a running time is reached.
 */
template < class Neighbor >
class moTimeContinuator: public moContinuator<Neighbor>
{
public:

    typedef typename Neighbor::EOT EOT;

    /**
     * Constructor
     * @param _max maximum running time (in second)
     * @param _verbose verbose mode true/false -> on/off
     */
    //moTimeContinuator(time_t _max, bool _verbose = true): max(_max), verbose(_verbose) {
    //moTimeContinuator(double _max, bool _verbose = true): max(_max), verbose(_verbose) {
    moTimeContinuator(long long _max, bool _verbose = true): max_dur(_max), verbose(_verbose) {
      external = false;
      //start = time(NULL);
      //start = clock();
      start = std::chrono::steady_clock::now();
    }


    /**
     * Synchronize the whole time with an external starting time
     * @param _externalStart external starting time
     */
    //virtual void setStartingTime(clock_t _externalStart) {
    virtual void setStartingTime(std::chrono::time_point<std::chrono::steady_clock> _externalStart) {
      external = true;
      start = _externalStart;
    }


    /**
     * To get the starting time
     * @return starting time
     */
    //virtual clock_t getStartingTime() {
    virtual std::chrono::time_point<std::chrono::steady_clock> getStartingTime() {
      return start;
    }


    /**
     * To set the maximum running time
     *
     * @param _maxTime maximum running time
     */
    //virtual void maxTime(clock_t _maxTime) {
    virtual void maxTime(long long _maxTime) {
      max_dur = _maxTime;
    }


    /**
     * Returns false when the running time is reached.
     * @param _sol the current solution
     */
    virtual bool operator() (EOT& _sol)
    {
        bool res;
        //time_t elapsed = (time_t) difftime(time(NULL), start);
        //clock_t elapsed = clock() - start;
        //res = ((double) elapsed)/CLOCKS_PER_SEC < max;
        auto end = std::chrono::steady_clock::now();
        std::chrono::duration<double> elapsed_seconds = end - start;
        auto dur = std::chrono::duration_cast<std::chrono::milliseconds>(elapsed_seconds).count();
        res = dur < max_dur;

        if (!res && verbose)
            //std::cout << "STOP in moTimeContinuator: Reached maximum time [" << ((double) elapsed)/CLOCKS_PER_SEC << "/" << max << "]" << std::endl;
            std::cout << "STOP in moTimeContinuator: Reached maximum time [" << dur << "/" << max_dur << "]" << std::endl;

        return res;
    }

    /**
     * reset the start time
     * @param _solution a solution
     */
    virtual void init(EOT & _solution) {
      if (!external)
        //start = clock();
        start = std::chrono::steady_clock::now();
    }


    /**
     * Class name
     */
    virtual std::string className(void) const
    {
        return "moTimeContinuator";
    }


private:

  /** maximum running time */
  //clock_t max;
  /** starting time */
  //clock_t start;
  std::chrono::time_point<std::chrono::steady_clock> start;
  // duration of the regression (milli second)
  long long max_dur;

  /** external start flag */
  bool external;
  /** verbose mode */
  bool verbose;

};

#endif
