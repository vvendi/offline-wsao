import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn import linear_model
import matplotlib.pyplot as plt
import sys
import os
import re
import random
import datetime
import argparse

def getOrderX(word, order):
    order1 = [int(x) * -2 + 1 for x in word]
    order2 = []
    order3 = []
    if order > 1:
        for i, x in enumerate(order1[:-1]):
            for y in order1[i+1:]:
                order2.append(x * y)
    if order > 2:
        for i, x in enumerate(order1[:-2]):
            for j, y in enumerate(order1[i+1:-1]):
                for z in order1[i+j+2:]:
                    order3.append(x * y * z)
    return order1, order2, order3

def getColumnOrderX(order, size):
    if order == 1:
        return ['x' + str(i) for i in range(1, size + 1)]
    elif order == 2:
        return ['x' + str(i) + '_' + str(j) for i in range(1, size) for j in range(i + 1, size + 1)]
    elif order == 3:
        return ['x' + str(i) + '_' + str(j) + '_' + str(k) for i in range(1, size - 1) for j in range(i + 1, size) for k in range(j + 1, size + 1)]

def readDf(filepath, order):
    df = pd.read_csv(filepath, sep="[ ]+", names=["f(x)", "n", "x"], engine="python")
    del df["n"]
    order1 = []
    order2 = []
    order3 = []
    for x in df["x"]:
        o1, o2, o3 = getOrderX(x, order)
        order1.append(o1)
        order2.append(o2)
        order3.append(o3)
    size = len(df["x"][0])
    df2 = pd.DataFrame(order1, columns=getColumnOrderX(1, size))
    df = df.join(df2)
    if order > 1:
        df2 = pd.DataFrame(order2, columns=getColumnOrderX(2, size))
        df = df.join(df2)
    if order > 2:
        df2 = pd.DataFrame(order3, columns=getColumnOrderX(3, size))
        df = df.join(df2)
    del df["x"]
    return df

def getSparseOrderX(word, order, lag):
    order1 = [int(x) * -2 + 1 for x in word]
    order2 = []
    order3 = []
    if order > 1:
        for i, x in enumerate(order1[:-1]):
            for y in order1[i+1:i+lag+1]:
                order2.append(x * y)
    if order > 2:
        for i, x in enumerate(order1[:-2]):
            for j, y in enumerate(order1[i+1:i+lag+1]):
                for z in order1[i+j+2:i+j+lag+2]:
                    order3.append(x * y * z)
    return order1, order2, order3

def getColumnSparseOrderX(order, size, lag):
    if order == 1:
        return ['x' + str(i) for i in range(1, size + 1)]
    elif order == 2:
        return ['x' + str(i) + '_' + str(j) for i in range(1, size) for j in range(i + 1, min(i + lag + 1, size + 1))]
    elif order == 3:
        return ['x' + str(i) + '_' + str(j) + '_' + str(k) for i in range(1, size - 1) for j in range(i + 1, min(i + lag + 1, size)) for k in range(j + 1, min(j + lag + 1, size + 1))]

def readSparseDf(filepath, order, lag):    
    df = pd.read_csv(filepath, sep="[ ]+", names=["f(x)", "n", "x"], engine="python")
    del df["n"]
    order1 = []
    order2 = []
    order3 = []
    for x in df["x"]:
        o1, o2, o3 = getSparseOrderX(x, order, lag)
        order1.append(o1)
        order2.append(o2)
        order3.append(o3)
    size = len(df["x"][0])
    df2 = pd.DataFrame(order1, columns=getColumnSparseOrderX(1, size, lag))
    df = df.join(df2)
    if order > 1:
        df2 = pd.DataFrame(order2, columns=getColumnSparseOrderX(2, size, lag))
        df = df.join(df2)
    if order > 2:
        df2 = pd.DataFrame(order3, columns=getColumnSparseOrderX(3, size, lag))
        df = df.join(df2)
    del df["x"]
    return df

def multi_surrogates_generator(filename, nbEval, nbSols, order, sparse, lag):
    if(sparse):
        resDir = "../instance/sparse_surrogates_" + str(nbSols) + "/"
        isExist = os.path.exists(resDir)
        if not isExist:
            os.makedirs(resDir)
        df = readSparseDf(filename, order, lag)
    else:
        resDir = "../instance/surrogates_" + str(nbSols) + "/"
        isExist = os.path.exists(resDir)
        if not isExist:
            os.makedirs(resDir)
        df = readDf(filename, order)
    for i in range(0, nbEval):
        X_train, _, y_train, _ = train_test_split(df.loc[:, df.columns != "f(x)"], df["f(x)"], train_size=nbSols, random_state=i)
        reg = linear_model.LassoLarsCV()
        print("Reg Fit n°" + str(i + 1))
        reg.fit(X_train, y_train)
        
        df2 = pd.DataFrame(reg.coef_, columns = ['coef'], index=df.loc[:, df.columns != "f(x)"].columns)
        
        with open(resDir + "reg" + str(i) + ".json", 'wt') as fout:
            fout.write('{"problem": {"type": "walsh", "date": "')
            fout.write(datetime.datetime.today().strftime('%d/%m/%Y'))
            fout.write('", "generator": "generator.py", "author": "VV", "n":108, "k": 2, ')
            if sparse:
                fout.write('"l": ' + str(lag) + ', ')
            fout.write('"seed":"0", "terms": [')
            fout.write('{"w":' + str(np.mean(y_train)) + ',"ids":[]}')
            for index, coef in df2.itertuples(index=True):
                fout.write(',{"w":' + str(coef) + ',"ids":' + str([int(s) - 1 for s in re.findall(r'\d+', index)]) + '}')                
            fout.write(']}}')

def main(argv):
    random.seed(0)
    
    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--filename", type=str, help="File of known solutions")
    parser.add_argument("-e", "--nbEval", type=int, help="Number of surrogates to create")
    parser.add_argument("-n", "--nbSols", type=int, help="Number of solutions to use in regression")
    parser.add_argument("-o", "--order", type=int, help="Order of walsh model to use")
    parser.add_argument("-s", "--notSparse", action="store_true", help="If not using a sparse model")
    parser.add_argument("-l", "--lag", type=int, help="Lag parameter of sparse surrogates")
    args = parser.parse_args()
    
    filename = args.filename if args.filename else "../data/4000_random_solutions.txt"
    nbEval = args.nbEval if args.nbEval else 30
    nbSols = args.nbSols if args.nbSols else 1080
    order = args.order if args.order else 2
    sparse = False if args.notSparse else True
    lag = args.lag if args.lag else 2
    
    multi_surrogates_generator(filename, nbEval, nbSols, order, sparse, lag)


if __name__ == '__main__':
	main(sys.argv[1:])
